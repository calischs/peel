# Hydrokinetic harvesting with folded structures

There is a growing interest in the energy community with <i>hydrokinetic</i> electricity generation, that is, extraction of energy from the movement of water.  In contrast to conventional hydroelectric generation (which constitutes the oldest power plants in the U.S.), these methods don't require significant hydraulic head (i.e., a height change of water).  This drastically increases the resource available, as nearly all sites in the U.S. with enough head for hydroelectric generation are already tapped.  Further, hydrokinetic generation doesn't require dams, which significantly reduces the ecological effect of infrastructure.

Many hydrokinetic machines look similar to wind turbines, which also extract energy from a moving fluid.  The 800x increase in fluid density, however, means that dramatically more energy is available in the flow of a given speed.  Many existing hydrokinetic machines, however, have a cut-in speed (i.e., minimum operating flow speed) that is too high to take advantage of most of the existing hydrokinetic resource.  For example, the U.S. Department of Energy estimates (<a href='https://www.energy.gov/sites/prod/files/2013/12/f5/riverine_hydrokinetic_resource_assessment_and_mapping.pdf'>Assessment and Mapping of the Riverine Hydrokinetic
Resource in the Continental United States </a>) estimates that the theoretically available riverine hydrokinetic resource (not counting tidal, which is of a similar magnitude) is 1400 TWh/year, but only 120 TWh/year is recoverable with current technology.  For reference, the annual electricity consumption of the U.S. is around 4000 TWh/year.

<img src='img/turbine-collage.png' height=200px>
<img src='img/collage.jpg' height=200px>

This cut-in speed is largely a function of size and mass of the hydrokinetic collector.  The figure above shows a sampling of hydrokinetic machines (from <a href='https://www.sciencedirect.com/science/article/pii/S1364032115015725'>Hydrokinetic energy conversion: Technology, research, and outlook</a> and <a href='https://www.sciencedirect.com/science/article/pii/S1364032116309522'>Renewable energy harvesting by vortex-induced motions: Review and benchmarking of technologies</a>).  Many simply don't work when scaled down.  One reason is that the relative size of frictional losses (e.g., in bearings) becomes much greater at smaller scales.  Another reason is that many electromechanical conversion methods (i.e., generators) are less efficient at smaller sizes.

This project aims to produce arrays of cm-scale hydrokinetic harvesters leveraging origami-inspired fabrication techniques.  The harvesting mechanisms will be elastically constrained, eliminating bearing surfaces and associated frictional losses.  The electromechanical conversion will likely use dialectric elastomers, a very inexpensive type of variable capacitance generator (details below).  These devices have been demonstrated to be miniaturizable with very high power densities.

A variety of hydrodynamic phenomena have been demonstrated for energy harvesting from flows, including fluttering, galloping, buffeting, autorotation, and vortex-induced-vibration.  A fantastic review is given in <a href='https://www.sciencedirect.com/science/article/pii/S1364032116309522'>Renewable energy harvesting by vortex-induced motions: Review and benchmarking of technologies</a>.  These categorizations are helpful, but not completely precise (e.g. some technologies are a blend of categories, etc.).  Nonetheless, fluttering (two degree-of-freedom resonance) is shown to have exceptionally high specific power (power per flow area), but a high cut-in speed, as currently implemented.  Buffeting (harvesting from vortices shed by a fixed bluff body) is shown to have exceptionally high efficiency (capturing at the ~60% Betz limit of the energy available in the flow).  We hope to use some mix of these phenomena in our harvesting device.

To illustrate a simple case, the video below shows a simple sewing thread in a flowing soap film.  If the thread is longer than a critical length (which depends on the flow velocity, density of fluid, density of thread, stiffness of the thread), a stable oscillatory limit cycle develops.  Shorter than the critical length and the thread is simply pulled straight backwards; significantly longer than the critical length and the thread enters a chaotic trajectory.  The size of the front attachment point also plays a critical role, as some buffeting occurs.

<img src='img/thread-free-small.mp4' width=600px>

A very simple schematic of leveraging this phenomenon is shown below, where a honeycomb is fabricated by folding flat sheets, but inside each honeycomb cell is a elastic member cantilevered in the downstream direction.  The flow runs through the honeycomb cells, the walls of which channel the flow over the elastic harvesting member.  The honeycomb scaffolding channels the generated power (charge, in the case of dielectric elastomers) to a collection circuit.

<img src='img/flapping-schematic-small.mp4' width=600px>

The above illustration is very similar to existing vortex induced motion harvesters, except multiplexed on a much smaller scale.  One issue with harvesters based on fluttering or buffeting is that they operate quite efficiently about a certain resonant flow speed, but much less efficiently about other speeds.  For instance, here is some high speed video of a fluttering plate operating off its resonant flow speed.  Some high amplitude oscillations, but clearly inefficient.

<img src='vid/flutter-off-resonance-small.mp4' width=600px>

Instead of the above, we propose a harvester that is still based on lift forces as opposed to drag (and hence stands a shot at approaching the betz limit), but uses a control input to operate as a driven oscillator, effectively harvesting at a variety of speeds.

<img src='img/peel-generator.jpg' width=600px>

A small electrode at the upstream edge of the moving harvesting electrode is a control input.  A small charge placed on this region will deflect the leading edge, causing the flow to peel the entire electrode apart.  This does mechanical work against the electrostatic pressure, causing voltage on the moving electrode to rise (if charge is held constant.  there are a number of operating modes possible, for instance constant voltage, constant charge, constant energy).  This energy can be siphoned off the moving electrode and it can be oppositely charged to restart the cycle on the opposite edge of the cell.  The control electrode is again charged to initiate the transition, and the harvesting electrode traverses back left. 

<img src='img/peel-diagram.jpg' width=600px>

<img src='img/bernouli-diagram.png' width=600px>

The flow speed and electrode length give upper and lower bounds on maximum possible operating frequencies.  Using the conservative lower bound, we can start to get a picture of the power harvesting potential:

<img src='img/power.png' width=600px>

The cartoon above doesn't yet incorporate the limit of coulomb forces overpowering the peeling force from the flow (this will depend a lot on electrode compliance), or the limits of dialectric breakdown (this wil depend a lot on the film thicknesses involved).  Importantly, it also doesn't incorporate the power required to initially charge the terminals.

We can start to calculate the coulomb forces for an entire patch as a way to upper bound the peeling forces:

<img src='img/coulomb.png' width=600px>

These curves are truncated at dialectric breakdown voltages (assuming 17 V/um breakdown).

I'm envisioning this entire harvester array produced roll-to-roll from PET (dielectric) and aluminum foil (traces and electrodes).  Because of the high voltages and low currents involved, the conductivity differences between copper and aluminum may be negligible.  These materials are not only cheap but also recyclable.

Making sure the peel starts from the primed leaded edge is an important consideration. As a first approximation, we can calculate the Bernouli forces from the flow constriction for a given electrode length and deflection angle:

<img src='img/bernouli-force.png' width=600px>





### Variable capacitance unimorphs

Assume the elementry isotropic form of Hooke's law:
```math
E e_{xx}= s_{xx}- \nu s_{yy}-\nu s_{zz}
```
```math
E e_{yy}= s_{yy}- \nu s_{zz}-\nu s_{xx}
```
```math
E e_{zz}= s_{zz}- \nu s_{xx}-\nu s_{yy}
```
If we have a film laminated to a bending substrate, we can calculate 
```math
e_{xx} = \frac{t_{substrate}+t}{2 R}
```
where $`R`$ is the assumed radius of curvature for the bend and $`t`$ is the film thickness.  We assume perfect lamination, so $`e_{yy}=0`$ and no forces normal to the surface so $`s_{zz}=0`$. Then
```math
s_{yy} = \nu s_{xx}
```
and 
```math
s_{xx} = \frac{E e_{xx}}{1-\nu^2}
```
Thus, we can solve to determine the strain in the thickness direction:
```math
e_{zz} = -\frac{\nu e_{xx}}{1-\nu} = -\frac{\nu (t_{substrate}+t)}{2R(1-\nu)}
```
This means
```math
\Delta t = -t\frac{\nu (t_{substrate}+t)}{2R(1-\nu)}
```
and so the new film thickness is
```math
t_R = t + \Delta t = t\left(1-\frac{\nu (t_{substrate}+t)}{2R(1-\nu)}\right)
```
The unstrained capacitance between electrodes on top and bottom of the film is 
```math
C_\infty = \frac{\epsilon l w}{t}
```
And so the strained capacitance is
```math
C_R = \frac{\epsilon l w}{t_R} = \frac{C_\infty}{1-\frac{\nu (t_{substrate}+t)}{2R(1-\nu)}}
```
That is, the relevant quantity to determine the factor by which capacitance changes is 
```math
1-\frac{\nu}{(1-\nu)}\frac{ (t_{substrate}+t)}{2R}
```
We would like this quantity to approach zero.  That is, 
```math
\frac{\nu}{(1-\nu)} \approx \frac{2R}{ (t_{substrate}+t)}
```
The left side of this equation is easily calculated -- we plot below.  For realistic values of Poisson ratio, this quantity is around 0.5. Thus, we want  
```math
t_{substrate}+t \approx 4R
```

<img src='img/poisson-effect-dimensionless.png' width=400px>


I think this shows capacitance change by stretching via bending is a bad idea!  At least with a substrate with sufficient bending stiffness.  Maybe this is an argument for mylar only.

So how to the dialectric elastomer unimorphs work?  Multilayer means an effectively larger $`t_{substrate}`$.  Capacitance change value is multiplied by number of layers, even though change factor is the same as single layer.  Does this relation imply the configuration is good for actuators looking to maximize stroke, but bad for generators looking to operate using a smallish stroke.


### Dielectric elastomer actuators

1. <a href='https://ieeexplore.ieee.org/document/7989501/'>A High Speed Soft Robot Based On Dielectric Elastomer Actuators, Mihai Duduta, David R. Clarke, Robert J. Wood</a>
    - unimorph dea for robots
2. <a href='https://onlinelibrary.wiley.com/doi/abs/10.1002/adma.201601842'>Multilayer Dielectric Elastomers for Fast, Programmable Actuation without Prestretch</a>
   - spin coating plus single walled carbon nanotubes for very thin layers of multilayer dea without electrode stiffening.
   - 40 W/kg power density
   - acrylic elastomer: Sartomer CN9021, CN9014, and CN9018
3. <a href='https://micro.seas.harvard.edu/papers/IROS10_Petralia.pdf'>Fabrication and analysis of dielectric-elastomer minimum-energy structures for highly-deformable soft robotic systems</a>
   - prestretched acrylic VHB
   - Wood lab early paper on DEAs
3. <a href='https://www.spiedigitallibrary.org/conference-proceedings-of-spie/5759/0000/Novel-multilayer-electrostatic-solid-state-actuators-with-elastic-dielectric-Invited/10.1117/12.604468.pdf'>Novel multilayer electrostatic solid state actuators with elastic dielectric</a>
   - automated production of many layer dea's via spin coating silicone elastomer and spraying carbon through a mask.
   - silicone elastomer: <a href='https://www.wacker.com/cms/en/products/product/product.jsp?product=12924'>Wacker ELASTOSIL P 7670</a>, <a href='https://gluespec.com/Materials/pottant-encapsulant/dow-corning/96-082-a-b-encapsulant'>Dow Corning 96-082</a>

### Dielectric elastomer generators

3. <a href='https://ieeexplore.ieee.org/document/5664794/'>Dielectric Elastomer Generators: How Much Energy Can Be Converted?</a>
    - upper limits on energy cycles, operation modes (constant charge, constant voltage, constant field)
    - 1 J/g/cycle limit
    - dialectric breakdown enhanced by strain.  See "Large-scale failure modes of dielectric elastomer actuators", G. Kofod.
4. <a href='https://www.ncbi.nlm.nih.gov/pubmed/28121135>Dielectric Elastomer Generator with Improved Energy Density and Conversion Efficiency Based on Polyurethane Composites</a>
    - additives to polyurethane to increase dielectric constant, decrease elastic modulus, and increase resulting energy density.
5. <a href='https://www.spiedigitallibrary.org/conference-proceedings-of-spie/4329/0000/Dielectric-elastomers-generator-mode-fundamentals-and-applications/10.1117/12.432640.full?SSO=1'>Dielectric elastomers: generator mode fundamentals and applications</a>
    - overview of operating cycle, testing

### Hydrodynamic energy harvesting

<img src='img/buffeting-turbine.png' width=300px>

6. <a href='https://www.sciencedirect.com/science/article/pii/S1364032116309522'>Renewable energy harvesting by vortex-induced motions: Review and
benchmarking of technologies</a>
   - Distinguishes flutter, transverse galloping, torsional galloping, buffeting, vortex-induced-vibration, and autorotation
   - The exact distinctions between these categories seems mainly historical.
   - Buffeting has extremely high extraction efficiency and low cost.  
   - Both forms of flutter have high power per swept area, but also a high cut-in speed.  This cut in speed is

```math
U_c = \frac{1.76 r}{B}\sqrt{\frac{m}{\pi\rho}(\omega_P^2 - \omega_H^2)}
```
   - where $`r`$ is the radius of gyration, $`m`$ is mass per unit length, $`B`$ is the structure width, $`\omega_P`$ is the angular frequency of pitch, and $`\omega_H`$ is the angular frequency of heave.  That is, small fluttering foils have lower cut-in.

7. <a href='https://www.sciencedirect.com/science/article/pii/S1877705817338006'>Polymeric flexible plate in the wake of a bluff body for energy harvesting</a>
   - verification of buffeting of a thin plate.
8. <a href='https://aip.scitation.org/doi/pdf/10.1063/1.4825467'>Study of thin flexible piezoelectric membrane for energy harvesting</a>
   - Buffeting
   - multielectrode configuration for extracting energy of wave in plate
9. <a href='https://aip.scitation.org/doi/abs/10.1063/1.4719704'>Hydroelastic response and energy harvesting potential of flexible piezoelectric beams
in viscous flow</a>
   - Buffeting
   - Good coupled modeling of hydrodynamics, nonlinear beam, piezoelectric.  
   - The strains seen are concerning for piezoelectric, but potentially good for DEA.
10. <a href='http://mafija.fmf.uni-lj.si/seminar/files/2011_2012/seminar-pop.pdf'>Flutter-mills: from destructive
phenomenon to the story of success</a>
   - Called flutter, but they seem to be talking about buffetting
   - Covers electromechanical conversion, but somewhat outdated.
   - Includes interesting helmholtz resonator, but this requires compressibility.  
11. <a href='https://www.sciencedirect.com/science/article/pii/S0960148114002213'>Low-head hydropower extraction based on torsional galloping</a>
   - torsional galloping
   - limited study, not very encouraging.
12. <a href=''>Marine current energy extraction through buffeting</a>
   - This is what's cited in the overview paper for buffeting, but it contains more details about packing density, etc.

### Hydrokinetic energy resources
13. <a href='https://www.sciencedirect.com/science/article/pii/S1364032114008624'>Hydrokinetic energy conversion systems: A technology status review</a>
   - hydrokinetic resources of tidal, wave, and river
   - massive resource, but technically recoverable portion limited by cut-in speed.
14. <a href='https://www.energy.gov/sites/prod/files/2013/12/f5/riverine_hydrokinetic_resource_assessment_and_mapping.pdf'>Assessment and Mapping of the Riverine Hydrokinetic
Resource in the Continental United States </a>
   - Methodology to estimate technically recoverably resource
   - 1400 TWh/year theoretical, 120 TWh/year recoverable with current technology.
   - Recoverable parts must exceed expert panels opinion about speed and depth: 0.5 m/s and 2 m, respectively.
   - Packing density also limits recoverability: Rows of devices were separated by a distance of 10 D where D is the device diameter. Devices in a given row were separated by 2 D. The device diameter was assumed to be 80% of the average depth in deployment area (at the 5 percentile flow).
15. <a href='https://www.sciencedirect.com/science/article/pii/S1364032114003992'>Conversion of lowland river flow kinetic energy</a>
   - "conveyor" type hydrokinetic machine specifically designed for low-speed, low-depth operation.  
   - Vanes pop in and out to spin a conveyor belt.
16. <a href='https://www.sciencedirect.com/science/article/pii/S1364032115015725'>Hydrokinetic energy conversion: Technology, research, and outlook</a>
   - understanding array interactions is important, individual turbines may exceed Betz limit.

### Other
17. <a href='https://www.sciencedirect.com/science/article/pii/S2211285517302021'>Nanocellulose-based conductive materials and their emerging applications in energy devices - A review</a>
   - Nanocellulose might replace SWCNT for DEAs?

