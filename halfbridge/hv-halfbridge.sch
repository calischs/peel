<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="8.5.1">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="no"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="no"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="no"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="no"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="no"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="no"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="no"/>
<layer number="110" name="110" color="7" fill="1" visible="no" active="no"/>
<layer number="111" name="111" color="7" fill="1" visible="no" active="no"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="no"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="no" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="no" active="yes"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="no"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="no" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="no" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="no" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="no"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="no"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="129" name="top_silk" color="7" fill="1" visible="no" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="no" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="no"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="no"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="DrillLegend" color="7" fill="1" visible="no" active="no"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="14" fill="1" visible="no" active="no"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="no"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="no"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="no"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="no"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="no"/>
<layer number="207" name="207bmp" color="15" fill="10" visible="no" active="no"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="no"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="no"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="no"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="OrgLBR" color="13" fill="1" visible="no" active="no"/>
<layer number="255" name="Accent" color="7" fill="1" visible="no" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="sec" urn="urn:adsk.eagle:library:725670">
<packages>
<package name="D2PAK" library_version="7" library_locally_modified="yes">
<smd name="P$1" x="0" y="0" dx="9.4" dy="10.8" layer="1"/>
<smd name="P$2" x="10" y="2.55" dx="3.8" dy="1.5" layer="1"/>
<smd name="P$3" x="10" y="-2.55" dx="3.8" dy="1.5" layer="1"/>
</package>
<package name="SOT23-6" library_version="7" library_locally_modified="yes">
<smd name="P$1" x="0" y="0" dx="0.35" dy="1" layer="1"/>
<smd name="P$2" x="0.95" y="0" dx="0.35" dy="1" layer="1"/>
<smd name="P$3" x="1.9" y="0" dx="0.35" dy="1" layer="1"/>
<smd name="P$6" x="0" y="2.8" dx="0.35" dy="1" layer="1"/>
<smd name="P$5" x="0.95" y="2.8" dx="0.35" dy="1" layer="1"/>
<smd name="P$4" x="1.9" y="2.8" dx="0.35" dy="1" layer="1"/>
<wire x1="-0.55" y1="2.3" x2="-0.55" y2="0.5" width="0.127" layer="21"/>
<wire x1="-0.55" y1="0.5" x2="2.45" y2="0.5" width="0.127" layer="21"/>
<wire x1="2.45" y1="0.5" x2="2.45" y2="2.3" width="0.127" layer="21"/>
<wire x1="2.45" y1="2.3" x2="-0.55" y2="2.3" width="0.127" layer="21"/>
<circle x="-0.95" y="0" radius="0.25" width="0" layer="21"/>
<text x="-3.8" y="1.9" size="0.5" layer="21">&gt;NAME</text>
<text x="-3.8" y="0.95" size="0.5" layer="21">&gt;VALUE</text>
</package>
<package name="DPAK" library_version="7" library_locally_modified="yes">
<smd name="P$1" x="3.3" y="-2.3" dx="3" dy="1.6" layer="1"/>
<smd name="P$2" x="3.3" y="2.3" dx="3" dy="1.6" layer="1"/>
<smd name="P$3" x="-3.35" y="0" dx="6.7" dy="6.7" layer="1"/>
</package>
<package name="RESC0201_L" library_version="7" library_locally_modified="yes">
<description>&lt;b&gt;0201&lt;/b&gt; chip&lt;p&gt;
0201 (imperial)&lt;br/&gt;
0603 (metric)&lt;br/&gt;
IPC High Density</description>
<smd name="1" x="-0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<smd name="2" x="0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<text x="-2" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-2" y="-2" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-0.3" y1="0.15" x2="-0.3" y2="-0.15" width="0.1" layer="51"/>
<wire x1="-0.3" y1="-0.15" x2="0.3" y2="-0.15" width="0.1" layer="51"/>
<wire x1="0.3" y1="-0.15" x2="0.3" y2="0.15" width="0.1" layer="51"/>
<wire x1="0.3" y1="0.15" x2="-0.3" y2="0.15" width="0.1" layer="51"/>
<wire x1="-0.5" y1="-0.3" x2="0.5" y2="-0.3" width="0.1" layer="39"/>
<wire x1="0.5" y1="-0.3" x2="0.5" y2="0.3" width="0.1" layer="39"/>
<wire x1="0.5" y1="0.3" x2="-0.5" y2="0.3" width="0.1" layer="39"/>
<wire x1="-0.5" y1="0.3" x2="-0.5" y2="-0.3" width="0.1" layer="39"/>
</package>
<package name="RESC0201_M" library_version="7" library_locally_modified="yes">
<description>&lt;b&gt;0201&lt;/b&gt; chip&lt;p&gt;
0201 (imperial)&lt;br/&gt;
0603 (metric)&lt;br/&gt;
IPC Low Density</description>
<smd name="1" x="-0.355" y="0" dx="0.5" dy="0.55" layer="1"/>
<smd name="2" x="0.355" y="0" dx="0.5" dy="0.55" layer="1"/>
<text x="-0.4" y="1.1" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-0.3" y1="0.15" x2="-0.3" y2="-0.15" width="0.1" layer="51"/>
<wire x1="-0.3" y1="-0.15" x2="0.3" y2="-0.15" width="0.1" layer="51"/>
<wire x1="0.3" y1="-0.15" x2="0.3" y2="0.15" width="0.1" layer="51"/>
<wire x1="0.3" y1="0.15" x2="-0.3" y2="0.15" width="0.1" layer="51"/>
<wire x1="0.8" y1="-0.5" x2="0.8" y2="0.5" width="0.1" layer="39"/>
<wire x1="0.8" y1="0.5" x2="-0.8" y2="0.5" width="0.1" layer="39"/>
<wire x1="-0.8" y1="0.5" x2="-0.8" y2="-0.5" width="0.1" layer="39"/>
<wire x1="-0.8" y1="-0.5" x2="0.8" y2="-0.5" width="0.1" layer="39"/>
</package>
<package name="RESC0201_N" library_version="7" library_locally_modified="yes">
<description>&lt;b&gt;0201&lt;/b&gt; chip&lt;p&gt;
0201 (imperial)&lt;br/&gt;
0603 (metric)&lt;br/&gt;
IPC Nominal Density</description>
<smd name="1" x="-0.305" y="0" dx="0.4" dy="0.45" layer="1"/>
<smd name="2" x="0.305" y="0" dx="0.4" dy="0.45" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-0.3" y1="0.15" x2="-0.3" y2="-0.15" width="0.1" layer="51"/>
<wire x1="-0.3" y1="-0.15" x2="0.3" y2="-0.15" width="0.1" layer="51"/>
<wire x1="0.3" y1="-0.15" x2="0.3" y2="0.15" width="0.1" layer="51"/>
<wire x1="0.3" y1="0.15" x2="-0.3" y2="0.15" width="0.1" layer="51"/>
<wire x1="0.65" y1="-0.35" x2="0.65" y2="0.35" width="0.1" layer="39"/>
<wire x1="0.65" y1="0.35" x2="-0.65" y2="0.35" width="0.1" layer="39"/>
<wire x1="-0.65" y1="0.35" x2="-0.65" y2="-0.35" width="0.1" layer="39"/>
<wire x1="-0.65" y1="-0.35" x2="0.65" y2="-0.35" width="0.1" layer="39"/>
</package>
<package name="RESC0402_L" library_version="7" library_locally_modified="yes">
<description>&lt;b&gt;0402&lt;/b&gt; chip &lt;p&gt;

0402 (imperial)&lt;br/&gt;
1005 (metric)&lt;br/&gt;
IPC High Density</description>
<smd name="1" x="-0.5" y="0" dx="0.5" dy="0.6" layer="1"/>
<smd name="2" x="0.5" y="0" dx="0.5" dy="0.6" layer="1"/>
<text x="-1" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-1" y="-2" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<wire x1="-0.5" y1="0.25" x2="0.5" y2="0.25" width="0.1" layer="51"/>
<wire x1="0.5" y1="0.25" x2="0.5" y2="-0.25" width="0.1" layer="51"/>
<wire x1="0.5" y1="-0.25" x2="-0.5" y2="-0.25" width="0.1" layer="51"/>
<wire x1="-0.5" y1="-0.25" x2="-0.5" y2="0.25" width="0.1" layer="51"/>
<wire x1="0.85" y1="-0.4" x2="0.85" y2="0.4" width="0.1" layer="39"/>
<wire x1="0.85" y1="0.4" x2="-0.85" y2="0.4" width="0.1" layer="39"/>
<wire x1="-0.85" y1="0.4" x2="-0.85" y2="-0.4" width="0.1" layer="39"/>
<wire x1="-0.85" y1="-0.4" x2="0.85" y2="-0.4" width="0.1" layer="39"/>
</package>
<package name="RESC0402_M" library_version="7" library_locally_modified="yes">
<description>&lt;b&gt;0402&lt;/b&gt; chip &lt;p&gt;

0402 (imperial)&lt;br/&gt;
1005 (metric)&lt;br/&gt;
IPC Low Density</description>
<smd name="1" x="-0.6" y="0" dx="0.7" dy="0.7" layer="1"/>
<smd name="2" x="0.6" y="0" dx="0.7" dy="0.7" layer="1"/>
<text x="-1" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-1" y="-2" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<wire x1="-0.5" y1="0.25" x2="0.5" y2="0.25" width="0.1" layer="51"/>
<wire x1="0.5" y1="0.25" x2="0.5" y2="-0.25" width="0.1" layer="51"/>
<wire x1="0.5" y1="-0.25" x2="-0.5" y2="-0.25" width="0.1" layer="51"/>
<wire x1="-0.5" y1="-0.25" x2="-0.5" y2="0.25" width="0.1" layer="51"/>
<wire x1="1.15" y1="-0.55" x2="1.15" y2="0.55" width="0.1" layer="39"/>
<wire x1="1.15" y1="0.55" x2="-1.15" y2="0.55" width="0.1" layer="39"/>
<wire x1="-1.15" y1="0.55" x2="-1.15" y2="-0.55" width="0.1" layer="39"/>
<wire x1="-1.15" y1="-0.55" x2="1.15" y2="-0.55" width="0.1" layer="39"/>
</package>
<package name="RESC0402_N" library_version="7" library_locally_modified="yes">
<description>&lt;b&gt;0402&lt;/b&gt; chip &lt;p&gt;

0402 (imperial)&lt;br/&gt;
1005 (metric)&lt;br/&gt;
IPC Nominal Density</description>
<smd name="1" x="-0.55" y="0" dx="0.6" dy="0.6" layer="1"/>
<smd name="2" x="0.55" y="0" dx="0.6" dy="0.6" layer="1"/>
<text x="-0.6" y="1.1" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.7" y="-2" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<wire x1="-0.5" y1="0.25" x2="0.5" y2="0.25" width="0.1" layer="51"/>
<wire x1="0.5" y1="0.25" x2="0.5" y2="-0.25" width="0.1" layer="51"/>
<wire x1="0.5" y1="-0.25" x2="-0.5" y2="-0.25" width="0.1" layer="51"/>
<wire x1="-0.5" y1="-0.25" x2="-0.5" y2="0.25" width="0.1" layer="51"/>
<wire x1="1" y1="-0.45" x2="1" y2="0.45" width="0.1" layer="39"/>
<wire x1="1" y1="0.45" x2="-1" y2="0.45" width="0.1" layer="39"/>
<wire x1="-1" y1="0.45" x2="-1" y2="-0.45" width="0.1" layer="39"/>
<wire x1="-1" y1="-0.45" x2="1" y2="-0.45" width="0.1" layer="39"/>
</package>
<package name="RESC0603_L" library_version="7" library_locally_modified="yes">
<description>&lt;b&gt;0603&lt;/b&gt; chip &lt;p&gt;

0603 (imperial)&lt;br/&gt;
1608 (metric)&lt;br/&gt;
IPC High Density</description>
<smd name="1" x="-0.7" y="0" dx="0.85" dy="0.8" layer="1"/>
<smd name="2" x="0.7" y="0" dx="0.85" dy="0.8" layer="1"/>
<text x="-1" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-1" y="-2" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<wire x1="-0.8" y1="0.4" x2="0.8" y2="0.4" width="0.127" layer="51"/>
<wire x1="0.8" y1="0.4" x2="0.8" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0.8" y1="-0.4" x2="-0.8" y2="-0.4" width="0.127" layer="51"/>
<wire x1="-0.8" y1="-0.4" x2="-0.8" y2="0.4" width="0.127" layer="51"/>
<wire x1="1.25" y1="-0.5" x2="1.25" y2="0.5" width="0.1" layer="39"/>
<wire x1="1.25" y1="0.5" x2="-1.25" y2="0.5" width="0.1" layer="39"/>
<wire x1="-1.25" y1="0.5" x2="-1.25" y2="-0.5" width="0.1" layer="39"/>
<wire x1="-1.25" y1="-0.5" x2="1.25" y2="-0.5" width="0.1" layer="39"/>
</package>
<package name="RESC0603_M" library_version="7" library_locally_modified="yes">
<description>&lt;b&gt;0603&lt;/b&gt; chip &lt;p&gt;

0603 (imperial)&lt;br/&gt;
1608 (metric)&lt;br/&gt;
IPC High Density</description>
<smd name="1" x="-0.9" y="0" dx="1.25" dy="1" layer="1"/>
<smd name="2" x="0.9" y="0" dx="1.25" dy="1" layer="1"/>
<text x="-1" y="2" size="1.27" layer="25">&gt;NAME</text>
<text x="-1" y="-3" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<wire x1="-0.8" y1="0.4" x2="0.8" y2="0.4" width="0.127" layer="51"/>
<wire x1="0.8" y1="0.4" x2="0.8" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0.8" y1="-0.4" x2="-0.8" y2="-0.4" width="0.127" layer="51"/>
<wire x1="-0.8" y1="-0.4" x2="-0.8" y2="0.4" width="0.127" layer="51"/>
<wire x1="2" y1="-1" x2="2" y2="1" width="0.1" layer="39"/>
<wire x1="2" y1="1" x2="-2" y2="1" width="0.1" layer="39"/>
<wire x1="-2" y1="1" x2="-2" y2="-1" width="0.1" layer="39"/>
<wire x1="-2" y1="-1" x2="2" y2="-1" width="0.1" layer="39"/>
</package>
<package name="RESC0603_N" library_version="7" library_locally_modified="yes">
<description>&lt;b&gt;0603&lt;/b&gt; chip &lt;p&gt;

0603 (imperial)&lt;br/&gt;
1608 (metric)&lt;br/&gt;
IPC Nominal Density</description>
<smd name="1" x="-0.8" y="0" dx="1.05" dy="0.9" layer="1"/>
<smd name="2" x="0.8" y="0" dx="1.05" dy="0.9" layer="1"/>
<text x="-1" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-1" y="-2.5" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<wire x1="-0.8" y1="0.4" x2="0.8" y2="0.4" width="0.127" layer="51"/>
<wire x1="0.8" y1="0.4" x2="0.8" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0.8" y1="-0.4" x2="-0.8" y2="-0.4" width="0.127" layer="51"/>
<wire x1="-0.8" y1="-0.4" x2="-0.8" y2="0.4" width="0.127" layer="51"/>
<wire x1="1.6" y1="-0.7" x2="1.6" y2="0.7" width="0.1" layer="39"/>
<wire x1="1.6" y1="0.7" x2="-1.6" y2="0.7" width="0.1" layer="39"/>
<wire x1="-1.6" y1="0.7" x2="-1.6" y2="-0.7" width="0.1" layer="39"/>
<wire x1="-1.6" y1="-0.7" x2="1.6" y2="-0.7" width="0.1" layer="39"/>
</package>
<package name="RESC0805_L" library_version="7" library_locally_modified="yes">
<description>&lt;b&gt;0805&lt;/b&gt;chip&lt;p&gt;

0805 (imperial)&lt;br/&gt;
2012 (metric)&lt;br/&gt;
IPC High Density</description>
<smd name="1" x="-1" y="0" dx="1" dy="1.25" layer="1"/>
<smd name="2" x="1" y="0" dx="1" dy="1.25" layer="1"/>
<text x="-1" y="1" size="0.508" layer="25">&gt;NAME</text>
<text x="-1" y="-1.23" size="0.508" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
<wire x1="-1" y1="0.6" x2="1" y2="0.6" width="0.127" layer="51"/>
<wire x1="1" y1="0.6" x2="1" y2="-0.6" width="0.127" layer="51"/>
<wire x1="1" y1="-0.6" x2="-1" y2="-0.6" width="0.127" layer="51"/>
<wire x1="-1" y1="-0.6" x2="-1" y2="0.6" width="0.127" layer="51"/>
<wire x1="1.6" y1="-0.75" x2="1.6" y2="0.75" width="0.1" layer="39"/>
<wire x1="1.6" y1="0.75" x2="-1.6" y2="0.75" width="0.1" layer="39"/>
<wire x1="-1.6" y1="0.75" x2="-1.6" y2="-0.75" width="0.1" layer="39"/>
<wire x1="-1.6" y1="-0.75" x2="1.6" y2="-0.75" width="0.1" layer="39"/>
</package>
<package name="RESC0805_M" library_version="7" library_locally_modified="yes">
<description>&lt;b&gt;0805&lt;/b&gt;chip&lt;p&gt;

0805 (imperial)&lt;br/&gt;
2012 (metric)&lt;br/&gt;
IPC Low Density</description>
<smd name="1" x="-1.2" y="0" dx="1.4" dy="1.45" layer="1"/>
<smd name="2" x="1.2" y="0" dx="1.4" dy="1.45" layer="1"/>
<text x="-2" y="0.73" size="0.508" layer="25">&gt;NAME</text>
<text x="-2" y="-1.73" size="0.508" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
<wire x1="-1" y1="0.6" x2="1" y2="0.6" width="0.127" layer="51"/>
<wire x1="1" y1="0.6" x2="1" y2="-0.6" width="0.127" layer="51"/>
<wire x1="1" y1="-0.6" x2="-1" y2="-0.6" width="0.127" layer="51"/>
<wire x1="-1" y1="-0.6" x2="-1" y2="0.6" width="0.127" layer="51"/>
<wire x1="2.4" y1="-1.2" x2="2.4" y2="1.2" width="0.1" layer="39"/>
<wire x1="2.4" y1="1.2" x2="-2.4" y2="1.2" width="0.1" layer="39"/>
<wire x1="-2.4" y1="1.2" x2="-2.4" y2="-1.2" width="0.1" layer="39"/>
<wire x1="-2.4" y1="-1.2" x2="2.4" y2="-1.2" width="0.1" layer="39"/>
</package>
<package name="RESC0805_N" library_version="7" library_locally_modified="yes">
<description>&lt;b&gt;0805&lt;/b&gt;chip&lt;p&gt;

0805 (imperial)&lt;br/&gt;
2012 (metric)&lt;br/&gt;
IPC Nominal Density</description>
<smd name="1" x="-1.1" y="0" dx="1.2" dy="1.35" layer="1"/>
<smd name="2" x="1" y="0" dx="1.2" dy="1.35" layer="1"/>
<text x="-1" y="0.738" size="0.508" layer="25">&gt;NAME</text>
<text x="-1" y="-1.23" size="0.508" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
<wire x1="-1" y1="0.6" x2="1" y2="0.6" width="0.127" layer="51"/>
<wire x1="1" y1="0.6" x2="1" y2="-0.6" width="0.127" layer="51"/>
<wire x1="1" y1="-0.6" x2="-1" y2="-0.6" width="0.127" layer="51"/>
<wire x1="-1" y1="-0.6" x2="-1" y2="0.6" width="0.127" layer="51"/>
<wire x1="1.85" y1="-0.95" x2="1.85" y2="0.95" width="0.1" layer="39"/>
<wire x1="1.85" y1="0.95" x2="-1.95" y2="0.95" width="0.1" layer="39"/>
<wire x1="-1.95" y1="0.95" x2="-1.95" y2="-0.95" width="0.1" layer="39"/>
<wire x1="-1.95" y1="-0.95" x2="1.85" y2="-0.95" width="0.1" layer="39"/>
</package>
<package name="1206_MILL" library_version="7" library_locally_modified="yes">
<description>&lt;b&gt;0805&lt;/b&gt;chip&lt;p&gt;

0805 (imperial)&lt;br/&gt;
2012 (metric)&lt;br/&gt;
IPC Nominal Density</description>
<smd name="1" x="-1.5875" y="0" dx="1.524" dy="2.032" layer="1"/>
<smd name="2" x="1.5875" y="0" dx="1.524" dy="2.032" layer="1"/>
<text x="-1" y="1.119" size="0.762" layer="25">&gt;NAME</text>
<text x="-1" y="-1.865" size="0.762" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
<wire x1="-1.5875" y1="0.762" x2="1.5875" y2="0.762" width="0.127" layer="51"/>
<wire x1="1.5875" y1="0.762" x2="1.5875" y2="-0.762" width="0.127" layer="51"/>
<wire x1="1.5875" y1="-0.762" x2="-1.5875" y2="-0.762" width="0.127" layer="51"/>
<wire x1="-1.5875" y1="-0.762" x2="-1.5875" y2="0.762" width="0.127" layer="51"/>
</package>
<package name="C0805" urn="urn:adsk.eagle:footprint:739321/1" locally_modified="yes" library_version="7" library_locally_modified="yes">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="0.889" size="0.6096" layer="25">&gt;NAME</text>
<text x="-1.27" y="-1.524" size="0.6096" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="2512" library_version="7" library_locally_modified="yes">
<smd name="P$1" x="-3.175" y="0" dx="1.6256" dy="3.175" layer="1" roundness="20"/>
<smd name="P$2" x="3.175" y="0" dx="1.6256" dy="3.175" layer="1" roundness="20"/>
<wire x1="-1.905" y1="1.27" x2="1.905" y2="1.27" width="0.127" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="1.905" y2="-1.27" width="0.127" layer="21"/>
<text x="-3.81" y="-2.54" size="0.635" layer="21">&gt;NAME</text>
<text x="0" y="-2.54" size="0.635" layer="21">&gt;VALUE</text>
</package>
<package name="BARREL_JACK_SMD" library_version="7" library_locally_modified="yes">
<smd name="P$1" x="4.8768" y="5.6896" dx="2.794" dy="2.54" layer="1"/>
<smd name="P$2" x="11.0998" y="5.6896" dx="2.794" dy="2.54" layer="1"/>
<smd name="P$3" x="11.0998" y="-5.6896" dx="2.794" dy="2.54" layer="1"/>
<smd name="P$4" x="4.8768" y="-5.6896" dx="2.794" dy="2.54" layer="1"/>
<wire x1="0" y1="-4.572" x2="14.732" y2="-4.572" width="0.127" layer="21"/>
<wire x1="14.732" y1="-4.572" x2="14.732" y2="4.572" width="0.127" layer="21"/>
<wire x1="14.732" y1="4.572" x2="0" y2="4.572" width="0.127" layer="21"/>
<wire x1="0" y1="4.572" x2="0" y2="-4.572" width="0.127" layer="21"/>
<hole x="5.0038" y="0" drill="1.6764"/>
<hole x="9.4996" y="0" drill="1.8796"/>
<circle x="5.0038" y="0" radius="0.889" width="0.127" layer="46"/>
<circle x="9.4996" y="0" radius="1.016" width="0.127" layer="46"/>
</package>
<package name="SOT23" library_version="7" library_locally_modified="yes">
<description>&lt;b&gt;SOT 23&lt;/b&gt;</description>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.1524" x2="-1.4224" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="-1.4224" y1="0.6604" x2="-0.8636" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.1524" width="0.1524" layer="21"/>
<wire x1="0.8636" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="21"/>
<smd name="3" x="0" y="1.2" dx="0.7" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="-1.2" dx="0.7" dy="1.6" layer="1"/>
<smd name="1" x="-0.95" y="-1.2" dx="0.7" dy="1.6" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
<package name="403D" library_version="7" library_locally_modified="yes">
<smd name="P$1" x="2" y="0" dx="2" dy="2" layer="1"/>
<smd name="P$2" x="-2" y="0" dx="2" dy="2" layer="1"/>
</package>
<package name="2220" library_version="7" library_locally_modified="yes">
<smd name="P$1" x="2.6" y="0" dx="2.5" dy="5.2" layer="1"/>
<smd name="P$2" x="-2.6" y="0" dx="2.5" dy="5.2" layer="1"/>
<text x="-3.81" y="2.921" size="1.27" layer="21">&gt;NAME</text>
<text x="-3.81" y="-4.191" size="1.27" layer="21">&gt;VALUE</text>
</package>
<package name="UWT_ALUMINUM_CAP" library_version="7" library_locally_modified="yes">
<smd name="P$1" x="0" y="-3.3" dx="3" dy="4.5" layer="1" roundness="20"/>
<smd name="P$2" x="0" y="3.3" dx="3" dy="4.5" layer="1" roundness="20"/>
<wire x1="-4" y1="4" x2="-4" y2="-3" width="0.127" layer="21"/>
<wire x1="-4" y1="-3" x2="-3" y2="-4" width="0.127" layer="21"/>
<wire x1="3" y1="-4" x2="4" y2="-3" width="0.127" layer="21"/>
<wire x1="4" y1="-3" x2="4" y2="4" width="0.127" layer="21"/>
<wire x1="4" y1="4" x2="2" y2="4" width="0.127" layer="21"/>
<wire x1="-4" y1="4" x2="-2" y2="4" width="0.127" layer="21"/>
<wire x1="-3" y1="-4" x2="-2" y2="-4" width="0.127" layer="21"/>
<wire x1="3" y1="-4" x2="2" y2="-4" width="0.127" layer="21"/>
</package>
<package name="TAG-CONNECT" library_version="7" library_locally_modified="yes">
<hole x="0" y="0" drill="0.99821875"/>
<hole x="5.08" y="1.016" drill="0.99821875"/>
<hole x="5.08" y="-1.016" drill="0.99821875"/>
<smd name="P$1" x="1.27" y="-0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100"/>
<smd name="P$2" x="1.27" y="0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100"/>
<smd name="P$4" x="2.54" y="0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100"/>
<smd name="P$6" x="3.81" y="0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100"/>
<smd name="P$5" x="3.81" y="-0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100"/>
<smd name="P$3" x="2.54" y="-0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100"/>
<wire x1="1.27" y1="-0.635" x2="3.81" y2="-0.635" width="0.127" layer="39"/>
<wire x1="3.81" y1="-0.635" x2="3.81" y2="0.635" width="0.127" layer="39"/>
<wire x1="3.81" y1="0.635" x2="1.27" y2="0.635" width="0.127" layer="39"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.127" layer="39"/>
<circle x="0" y="0" radius="0.5334" width="0.066040625" layer="46"/>
<circle x="0" y="0" radius="0.5334" width="0.066040625" layer="46"/>
<circle x="5.08" y="1.016" radius="0.5334" width="0.066040625" layer="46"/>
<circle x="5.08" y="-1.016" radius="0.5334" width="0.066040625" layer="46"/>
</package>
<package name="DO-214AC" urn="urn:adsk.eagle:footprint:43216/1" library_version="7" library_locally_modified="yes">
<description>&lt;b&gt;SURFACE MOUNT GENERAL RECTIFIER&lt;/b&gt; JEDEC DO-214AC molded platic body&lt;p&gt;
Method 2026&lt;br&gt;
Source: http://www.kingtronics.com/SMD_M7/M7_SMD_4007.pdf</description>
<wire x1="-2.15" y1="1.3" x2="2.15" y2="1.3" width="0.2032" layer="51"/>
<wire x1="2.15" y1="1.3" x2="2.15" y2="-1.3" width="0.2032" layer="51"/>
<wire x1="2.15" y1="-1.3" x2="-2.15" y2="-1.3" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.3" x2="-2.15" y2="1.3" width="0.2032" layer="51"/>
<wire x1="-1.035" y1="1.3" x2="1.025" y2="1.3" width="0.2032" layer="21"/>
<wire x1="1.025" y1="-1.3" x2="-1.035" y2="-1.3" width="0.2032" layer="21"/>
<smd name="C" x="-2.025" y="0" dx="1.8" dy="2.4" layer="1"/>
<smd name="A" x="2.025" y="0" dx="1.8" dy="2.4" layer="1" rot="R180"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.825" y1="-1.1" x2="-2.175" y2="1.1" layer="51"/>
<rectangle x1="2.175" y1="-1.1" x2="2.825" y2="1.1" layer="51" rot="R180"/>
<rectangle x1="-1.065" y1="-1.225" x2="-0.39" y2="1.225" layer="21"/>
<rectangle x1="-1.75" y1="-1.225" x2="-1.075" y2="1.225" layer="51"/>
</package>
<package name="HEADER_4POS_254" library_version="7" library_locally_modified="yes">
<smd name="P$1" x="0" y="3.81" dx="3.048" dy="1.524" layer="1"/>
<smd name="P$2" x="0" y="1.27" dx="3.048" dy="1.524" layer="1"/>
<smd name="P$3" x="0" y="-1.27" dx="3.048" dy="1.524" layer="1"/>
<smd name="P$4" x="0" y="-3.81" dx="3.048" dy="1.524" layer="1"/>
</package>
<package name="HEADER_3POS_254" library_version="7" library_locally_modified="yes">
<smd name="P$1" x="0" y="2.54" dx="3.048" dy="1.524" layer="1"/>
<smd name="P$2" x="0" y="0" dx="3.048" dy="1.524" layer="1"/>
<smd name="P$3" x="0" y="-2.54" dx="3.048" dy="1.524" layer="1"/>
</package>
<package name="SMA" library_version="7" library_locally_modified="yes">
<smd name="P$1" x="2" y="0" dx="2" dy="2" layer="1"/>
<smd name="P$2" x="-2" y="0" dx="2" dy="2" layer="1"/>
<text x="-3.048" y="1.397" size="1.27" layer="21">&gt;NAME</text>
<text x="-3.302" y="-2.667" size="1.27" layer="21">&gt;VALUE</text>
</package>
<package name="HEADER_4POS_254_THIN" library_version="7" library_locally_modified="yes">
<smd name="P$1" x="0" y="3.81" dx="3.048" dy="1.016" layer="1"/>
<smd name="P$2" x="0" y="1.27" dx="3.048" dy="1.016" layer="1"/>
<smd name="P$3" x="0" y="-1.27" dx="3.048" dy="1.016" layer="1"/>
<smd name="P$4" x="0" y="-3.81" dx="3.048" dy="1.016" layer="1"/>
</package>
<package name="HEADER_3POS_254_THIN" library_version="7" library_locally_modified="yes">
<smd name="P$1" x="0" y="2.54" dx="3.048" dy="1.016" layer="1"/>
<smd name="P$2" x="0" y="0" dx="3.048" dy="1.016" layer="1"/>
<smd name="P$3" x="0" y="-2.54" dx="3.048" dy="1.016" layer="1"/>
</package>
<package name="HOLE_M3" library_version="7" library_locally_modified="yes">
<hole x="0" y="0" drill="3.2"/>
<circle x="0" y="0" radius="1.65" width="0.1" layer="46"/>
<circle x="0" y="0" radius="2.75" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="PMOSFET" library_version="7" library_locally_modified="yes">
<circle x="-20.32" y="10.16" radius="10.16" width="0.254" layer="94"/>
<pin name="G" x="-33.02" y="7.62" length="middle"/>
<pin name="S" x="-15.24" y="22.86" length="middle" rot="R270"/>
<pin name="D" x="-15.24" y="-2.54" length="middle" rot="R90"/>
<wire x1="-15.24" y1="2.54" x2="-20.32" y2="5.08" width="0.254" layer="94"/>
<wire x1="-20.32" y1="15.24" x2="-15.24" y2="17.78" width="0.254" layer="94"/>
<wire x1="-27.94" y1="7.62" x2="-22.86" y2="7.62" width="0.254" layer="94"/>
<wire x1="-22.86" y1="7.62" x2="-22.86" y2="15.24" width="0.254" layer="94"/>
<wire x1="-22.86" y1="5.08" x2="-22.86" y2="7.62" width="0.254" layer="94"/>
<wire x1="-20.32" y1="5.08" x2="-20.32" y2="7.62" width="0.254" layer="94"/>
<wire x1="-20.32" y1="15.24" x2="-20.32" y2="12.7" width="0.254" layer="94"/>
<wire x1="-20.32" y1="8.636" x2="-20.32" y2="10.16" width="0.254" layer="94"/>
<wire x1="-20.32" y1="10.16" x2="-20.32" y2="11.684" width="0.254" layer="94"/>
<wire x1="-20.32" y1="15.24" x2="-20.32" y2="16.002" width="0.254" layer="94"/>
<wire x1="-20.32" y1="5.08" x2="-20.32" y2="4.318" width="0.254" layer="94"/>
<wire x1="-15.24" y1="10.16" x2="-17.78" y2="10.16" width="0.254" layer="94"/>
<wire x1="-17.78" y1="10.16" x2="-20.32" y2="10.16" width="0.254" layer="94"/>
<wire x1="-17.78" y1="10.16" x2="-19.05" y2="8.89" width="0.254" layer="94"/>
<wire x1="-19.05" y1="8.89" x2="-19.05" y2="11.43" width="0.254" layer="94"/>
<wire x1="-19.05" y1="11.43" x2="-17.78" y2="10.16" width="0.254" layer="94"/>
<wire x1="-15.24" y1="10.16" x2="-15.24" y2="17.78" width="0.254" layer="94"/>
</symbol>
<symbol name="UCC27511A_GATE_DRIVER" library_version="7" library_locally_modified="yes">
<pin name="VDD" x="-12.7" y="5.08" length="middle"/>
<pin name="OUTH" x="-12.7" y="0" length="middle"/>
<pin name="OUTL" x="-12.7" y="-5.08" length="middle"/>
<pin name="GND" x="12.7" y="-5.08" length="middle" rot="R180"/>
<pin name="IN-" x="12.7" y="0" length="middle" rot="R180"/>
<pin name="IN+" x="12.7" y="5.08" length="middle" rot="R180"/>
<wire x1="-10.16" y1="-7.62" x2="10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="7.62" x2="-10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="7.62" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
</symbol>
<symbol name="NMOSFET" library_version="7" library_locally_modified="yes">
<circle x="-20.32" y="10.16" radius="10.16" width="0.254" layer="94"/>
<pin name="G" x="-33.02" y="7.62" length="middle"/>
<pin name="D" x="-15.24" y="22.86" length="middle" rot="R270"/>
<pin name="S" x="-15.24" y="-2.54" length="middle" rot="R90"/>
<wire x1="-15.24" y1="2.54" x2="-20.32" y2="5.08" width="0.254" layer="94"/>
<wire x1="-20.32" y1="15.24" x2="-15.24" y2="17.78" width="0.254" layer="94"/>
<wire x1="-27.94" y1="7.62" x2="-22.86" y2="7.62" width="0.254" layer="94"/>
<wire x1="-22.86" y1="7.62" x2="-22.86" y2="15.24" width="0.254" layer="94"/>
<wire x1="-22.86" y1="5.08" x2="-22.86" y2="7.62" width="0.254" layer="94"/>
<wire x1="-20.32" y1="5.08" x2="-20.32" y2="7.62" width="0.254" layer="94"/>
<wire x1="-20.32" y1="15.24" x2="-20.32" y2="12.7" width="0.254" layer="94"/>
<wire x1="-20.32" y1="8.636" x2="-20.32" y2="10.16" width="0.254" layer="94"/>
<wire x1="-20.32" y1="10.16" x2="-20.32" y2="11.684" width="0.254" layer="94"/>
<wire x1="-20.32" y1="15.24" x2="-20.32" y2="16.002" width="0.254" layer="94"/>
<wire x1="-20.32" y1="5.08" x2="-20.32" y2="4.318" width="0.254" layer="94"/>
<wire x1="-15.24" y1="2.54" x2="-15.24" y2="10.16" width="0.254" layer="94"/>
<wire x1="-15.24" y1="10.16" x2="-19.05" y2="10.16" width="0.254" layer="94"/>
<wire x1="-19.05" y1="10.16" x2="-20.32" y2="10.16" width="0.254" layer="94"/>
<wire x1="-19.05" y1="10.16" x2="-17.78" y2="8.89" width="0.254" layer="94"/>
<wire x1="-17.78" y1="8.89" x2="-17.78" y2="11.43" width="0.254" layer="94"/>
<wire x1="-17.78" y1="11.43" x2="-19.05" y2="10.16" width="0.254" layer="94"/>
</symbol>
<symbol name="C-EU" library_version="7" library_locally_modified="yes">
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<text x="1.524" y="0.381" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-4.699" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="R-US" library_version="7" library_locally_modified="yes">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="+48V" library_version="7" library_locally_modified="yes">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+48V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+12V" library_version="7" library_locally_modified="yes">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+48V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="DUAL_POWER" library_version="7" library_locally_modified="yes">
<pin name="VCC1" x="-15.24" y="17.78" length="middle"/>
<pin name="VCC2" x="-15.24" y="12.7" length="middle"/>
<pin name="GND1" x="-15.24" y="7.62" length="middle"/>
<pin name="GND2" x="-15.24" y="2.54" length="middle"/>
<wire x1="-10.16" y1="20.32" x2="-10.16" y2="0" width="0.254" layer="94"/>
<wire x1="-10.16" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="20.32" width="0.254" layer="94"/>
<wire x1="0" y1="20.32" x2="-10.16" y2="20.32" width="0.254" layer="94"/>
</symbol>
<symbol name="REGULATOR" library_version="7" library_locally_modified="yes">
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="2.54" x2="-6.35" y2="-1.27" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="-1.27" x2="0" y2="-1.27" width="0.4064" layer="94"/>
<wire x1="0" y1="-1.27" x2="6.35" y2="-1.27" width="0.4064" layer="94"/>
<wire x1="6.35" y1="-1.27" x2="6.35" y2="2.54" width="0.4064" layer="94"/>
<wire x1="6.35" y1="2.54" x2="6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="6.35" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="2.54" x2="-6.35" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="6.35" y1="2.54" x2="7.62" y2="2.54" width="0.254" layer="94"/>
<text x="-6.35" y="-3.81" size="1.27" layer="95">&gt;NAME</text>
<text x="1.27" y="-3.81" size="1.27" layer="96">&gt;VALUE</text>
<pin name="IN" x="-7.62" y="2.54" length="point"/>
<pin name="GND" x="0" y="-2.54" length="point" rot="R90"/>
<pin name="OUT" x="7.62" y="2.54" length="point" rot="R180"/>
</symbol>
<symbol name="ZD" library_version="7" library_locally_modified="yes">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="0.635" y2="-1.27" width="0.254" layer="94"/>
<text x="-1.778" y="1.905" size="1.778" layer="95">&gt;NAME</text>
<text x="-1.778" y="-3.429" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="C-US" urn="urn:adsk.eagle:symbol:739320/1" library_version="7" library_locally_modified="yes">
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-1.016" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1" x2="2.4892" y2="-1.8542" width="0.254" layer="94" curve="-37.878202" cap="flat"/>
<wire x1="-2.4668" y1="-1.8504" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.373024" cap="flat"/>
<text x="1.016" y="0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.016" y="-4.191" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="GND" library_version="7" library_locally_modified="yes">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="PDI" library_version="7" library_locally_modified="yes">
<pin name="VCC" x="-12.7" y="7.62" length="middle"/>
<pin name="DAT" x="-12.7" y="2.54" length="middle"/>
<pin name="CLK" x="-12.7" y="-2.54" length="middle"/>
<pin name="GND" x="12.7" y="7.62" length="middle" rot="R180"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="-7.62" y1="10.16" x2="7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="10.16" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<text x="-7.62" y="-7.62" size="1.778" layer="95">PDI</text>
</symbol>
<symbol name="D" library_version="7" library_locally_modified="yes">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<text x="2.54" y="0.4826" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.3114" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.54" y="0" size="0.4064" layer="99" align="center">SpiceOrder 1</text>
<text x="2.54" y="0" size="0.4064" layer="99" align="center">SpiceOrder 2</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="+3V3" library_version="7" library_locally_modified="yes">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="HEADER_4POS" library_version="7" library_locally_modified="yes">
<pin name="P$1" x="-2.54" y="2.54" length="middle"/>
<pin name="P$2" x="-2.54" y="-2.54" length="middle"/>
<wire x1="0" y1="5.08" x2="0" y2="-15.24" width="0.254" layer="94"/>
<wire x1="0" y1="-15.24" x2="15.24" y2="-15.24" width="0.254" layer="94"/>
<wire x1="15.24" y1="-15.24" x2="15.24" y2="5.08" width="0.254" layer="94"/>
<wire x1="15.24" y1="5.08" x2="0" y2="5.08" width="0.254" layer="94"/>
<pin name="P$3" x="-2.54" y="-7.62" length="middle"/>
<pin name="P$4" x="-2.54" y="-12.7" length="middle"/>
</symbol>
<symbol name="HEADER_3POS" library_version="7" library_locally_modified="yes">
<pin name="P$1" x="-2.54" y="2.54" length="middle"/>
<pin name="P$2" x="-2.54" y="-2.54" length="middle"/>
<wire x1="0" y1="5.08" x2="0" y2="-10.16" width="0.254" layer="94"/>
<wire x1="0" y1="-10.16" x2="15.24" y2="-10.16" width="0.254" layer="94"/>
<wire x1="15.24" y1="-10.16" x2="15.24" y2="5.08" width="0.254" layer="94"/>
<wire x1="15.24" y1="5.08" x2="0" y2="5.08" width="0.254" layer="94"/>
<pin name="P$3" x="-2.54" y="-7.62" length="middle"/>
</symbol>
<symbol name="HOLE" library_version="7" library_locally_modified="yes">
<circle x="0" y="0" radius="2.54" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FAIRCHILD_500V_PMOS" library_version="7" library_locally_modified="yes">
<gates>
<gate name="G$1" symbol="PMOSFET" x="22.86" y="-7.62"/>
</gates>
<devices>
<device name="" package="D2PAK">
<connects>
<connect gate="G$1" pin="D" pad="P$1"/>
<connect gate="G$1" pin="G" pad="P$3"/>
<connect gate="G$1" pin="S" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="UCC27511A_GATE_DRIVER" library_version="7" library_locally_modified="yes">
<gates>
<gate name="G$1" symbol="UCC27511A_GATE_DRIVER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23-6">
<connects>
<connect gate="G$1" pin="GND" pad="P$4"/>
<connect gate="G$1" pin="IN+" pad="P$6"/>
<connect gate="G$1" pin="IN-" pad="P$5"/>
<connect gate="G$1" pin="OUTH" pad="P$2"/>
<connect gate="G$1" pin="OUTL" pad="P$3"/>
<connect gate="G$1" pin="VDD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ST_600V_NMOS" library_version="7" library_locally_modified="yes">
<gates>
<gate name="G$1" symbol="NMOSFET" x="22.86" y="-7.62"/>
</gates>
<devices>
<device name="" package="DPAK">
<connects>
<connect gate="G$1" pin="D" pad="P$3"/>
<connect gate="G$1" pin="G" pad="P$1"/>
<connect gate="G$1" pin="S" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAPACITOR" uservalue="yes" library_version="7" library_locally_modified="yes">
<description>&lt;b&gt;Generic chip capacitor&lt;/b&gt;</description>
<gates>
<gate name="C$1" symbol="C-EU" x="0" y="0"/>
</gates>
<devices>
<device name="_0201_L" package="RESC0201_L">
<connects>
<connect gate="C$1" pin="1" pad="1"/>
<connect gate="C$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0201_M" package="RESC0201_M">
<connects>
<connect gate="C$1" pin="1" pad="1"/>
<connect gate="C$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0201_N" package="RESC0201_N">
<connects>
<connect gate="C$1" pin="1" pad="1"/>
<connect gate="C$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0402_L" package="RESC0402_L">
<connects>
<connect gate="C$1" pin="1" pad="1"/>
<connect gate="C$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0402_M" package="RESC0402_M">
<connects>
<connect gate="C$1" pin="1" pad="1"/>
<connect gate="C$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0402_N" package="RESC0402_N">
<connects>
<connect gate="C$1" pin="1" pad="1"/>
<connect gate="C$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0603_L" package="RESC0603_L">
<connects>
<connect gate="C$1" pin="1" pad="1"/>
<connect gate="C$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0603_M" package="RESC0603_M">
<connects>
<connect gate="C$1" pin="1" pad="1"/>
<connect gate="C$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0603_N" package="RESC0603_N">
<connects>
<connect gate="C$1" pin="1" pad="1"/>
<connect gate="C$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0805_L" package="RESC0805_L">
<connects>
<connect gate="C$1" pin="1" pad="1"/>
<connect gate="C$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0805_M" package="RESC0805_M">
<connects>
<connect gate="C$1" pin="1" pad="1"/>
<connect gate="C$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0805_N" package="RESC0805_N">
<connects>
<connect gate="C$1" pin="1" pad="1"/>
<connect gate="C$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="1206_MILL">
<connects>
<connect gate="C$1" pin="1" pad="1"/>
<connect gate="C$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2220" package="2220">
<connects>
<connect gate="C$1" pin="1" pad="P$1"/>
<connect gate="C$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR" library_version="7" library_locally_modified="yes">
<gates>
<gate name="G$1" symbol="R-US" x="0" y="0"/>
</gates>
<devices>
<device name="0805" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="1206_MILL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="2512">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+48V" prefix="+12V" library_version="7" library_locally_modified="yes">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+48V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+12V" prefix="+12V" library_version="7" library_locally_modified="yes">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+12V" x="0" y="2.54"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="POWER_BARREL_JACK" library_version="7" library_locally_modified="yes">
<gates>
<gate name="G$1" symbol="DUAL_POWER" x="12.7" y="-10.16"/>
</gates>
<devices>
<device name="" package="BARREL_JACK_SMD">
<connects>
<connect gate="G$1" pin="GND1" pad="P$3"/>
<connect gate="G$1" pin="GND2" pad="P$4"/>
<connect gate="G$1" pin="VCC1" pad="P$1"/>
<connect gate="G$1" pin="VCC2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="REGULATOR" prefix="IC" uservalue="yes" library_version="7" library_locally_modified="yes">
<gates>
<gate name="G$1" symbol="REGULATOR" x="0" y="0"/>
</gates>
<devices>
<device name="SOT23" package="SOT23">
<connects>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="IN" pad="2"/>
<connect gate="G$1" pin="OUT" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ZENER_ON_1SMA59XXBT3G" library_version="7" library_locally_modified="yes">
<gates>
<gate name="G$1" symbol="ZD" x="0" y="0"/>
</gates>
<devices>
<device name="" package="403D">
<connects>
<connect gate="G$1" pin="A" pad="P$1"/>
<connect gate="G$1" pin="C" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAPACITOR_ALUM" library_version="7" library_locally_modified="yes">
<gates>
<gate name="G$1" symbol="C-US" x="0" y="0"/>
</gates>
<devices>
<device name="" package="UWT_ALUMINUM_CAP">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND" library_version="7" library_locally_modified="yes">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TAGCONNECT_PDI" library_version="7" library_locally_modified="yes">
<gates>
<gate name="G$1" symbol="PDI" x="7.62" y="5.08"/>
</gates>
<devices>
<device name="" package="TAG-CONNECT">
<connects>
<connect gate="G$1" pin="CLK" pad="P$1"/>
<connect gate="G$1" pin="DAT" pad="P$5"/>
<connect gate="G$1" pin="GND" pad="P$6"/>
<connect gate="G$1" pin="VCC" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE_ES1F_FAIRCHILD" library_version="7" library_locally_modified="yes">
<gates>
<gate name="G$1" symbol="D" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DO-214AC">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3V3" prefix="+3V3" library_version="7" library_locally_modified="yes">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HEADER_4POS_254" library_version="7" library_locally_modified="yes">
<gates>
<gate name="G$1" symbol="HEADER_4POS" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HEADER_4POS_254">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
<connect gate="G$1" pin="P$4" pad="P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="THIN" package="HEADER_4POS_254_THIN">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
<connect gate="G$1" pin="P$4" pad="P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HEADER_3POS_254" library_version="7" library_locally_modified="yes">
<gates>
<gate name="G$1" symbol="HEADER_3POS" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HEADER_3POS_254">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="THIN" package="HEADER_3POS_254_THIN">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE_HV_STTH112" library_version="7" library_locally_modified="yes">
<gates>
<gate name="G$1" symbol="D" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SMA">
<connects>
<connect gate="G$1" pin="A" pad="P$1"/>
<connect gate="G$1" pin="C" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HOLE_M3" library_version="7" library_locally_modified="yes">
<gates>
<gate name="G$1" symbol="HOLE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HOLE_M3">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="fab">
<packages>
<package name="TQFP44-13THIN">
<description>&lt;b&gt;Thin Quad Flat Pack&lt;/b&gt;&lt;p&gt;
package type TQ</description>
<wire x1="-4.8" y1="4.4" x2="-4.4" y2="4.8" width="0.2032" layer="21"/>
<wire x1="-4.4" y1="4.8" x2="4.4" y2="4.8" width="0.2032" layer="21"/>
<wire x1="4.4" y1="4.8" x2="4.8" y2="4.4" width="0.2032" layer="21"/>
<wire x1="4.8" y1="4.4" x2="4.8" y2="-4.4" width="0.2032" layer="21"/>
<wire x1="4.8" y1="-4.4" x2="4.4" y2="-4.8" width="0.2032" layer="21"/>
<wire x1="4.4" y1="-4.8" x2="-4.4" y2="-4.8" width="0.2032" layer="21"/>
<wire x1="-4.4" y1="-4.8" x2="-4.8" y2="-4.4" width="0.2032" layer="21"/>
<wire x1="-4.8" y1="-4.4" x2="-4.8" y2="4.4" width="0.2032" layer="21"/>
<circle x="-4" y="4" radius="0.2827" width="0.254" layer="21"/>
<smd name="1" x="-5.8" y="4" dx="1.524" dy="0.3302" layer="1"/>
<smd name="2" x="-5.8" y="3.2" dx="1.524" dy="0.3302" layer="1"/>
<smd name="3" x="-5.8" y="2.4" dx="1.524" dy="0.3302" layer="1"/>
<smd name="4" x="-5.8" y="1.6" dx="1.524" dy="0.3302" layer="1"/>
<smd name="5" x="-5.8" y="0.8" dx="1.524" dy="0.3302" layer="1"/>
<smd name="6" x="-5.8" y="0" dx="1.524" dy="0.3302" layer="1"/>
<smd name="7" x="-5.8" y="-0.8" dx="1.524" dy="0.3302" layer="1"/>
<smd name="8" x="-5.8" y="-1.6" dx="1.524" dy="0.3302" layer="1"/>
<smd name="9" x="-5.8" y="-2.4" dx="1.524" dy="0.3302" layer="1"/>
<smd name="10" x="-5.8" y="-3.2" dx="1.524" dy="0.3302" layer="1"/>
<smd name="11" x="-5.8" y="-4" dx="1.524" dy="0.3302" layer="1"/>
<smd name="12" x="-4" y="-5.8" dx="0.3302" dy="1.524" layer="1"/>
<smd name="13" x="-3.2" y="-5.8" dx="0.3302" dy="1.524" layer="1"/>
<smd name="14" x="-2.4" y="-5.8" dx="0.3302" dy="1.524" layer="1"/>
<smd name="15" x="-1.6" y="-5.8" dx="0.3302" dy="1.524" layer="1"/>
<smd name="16" x="-0.8" y="-5.8" dx="0.3302" dy="1.524" layer="1"/>
<smd name="17" x="0" y="-5.8" dx="0.3302" dy="1.524" layer="1"/>
<smd name="18" x="0.8" y="-5.8" dx="0.3302" dy="1.524" layer="1"/>
<smd name="19" x="1.6" y="-5.8" dx="0.3302" dy="1.524" layer="1"/>
<smd name="20" x="2.4" y="-5.8" dx="0.3302" dy="1.524" layer="1"/>
<smd name="21" x="3.2" y="-5.8" dx="0.3302" dy="1.524" layer="1"/>
<smd name="22" x="4" y="-5.8" dx="0.3302" dy="1.524" layer="1"/>
<smd name="23" x="5.8" y="-4" dx="1.524" dy="0.3302" layer="1"/>
<smd name="24" x="5.8" y="-3.2" dx="1.524" dy="0.3302" layer="1"/>
<smd name="25" x="5.8" y="-2.4" dx="1.524" dy="0.3302" layer="1"/>
<smd name="26" x="5.8" y="-1.6" dx="1.524" dy="0.3302" layer="1"/>
<smd name="27" x="5.8" y="-0.8" dx="1.524" dy="0.3302" layer="1"/>
<smd name="28" x="5.8" y="0" dx="1.524" dy="0.3302" layer="1"/>
<smd name="29" x="5.8" y="0.8" dx="1.524" dy="0.3302" layer="1"/>
<smd name="30" x="5.8" y="1.6" dx="1.524" dy="0.3302" layer="1"/>
<smd name="31" x="5.8" y="2.4" dx="1.524" dy="0.3302" layer="1"/>
<smd name="32" x="5.8" y="3.2" dx="1.524" dy="0.3302" layer="1"/>
<smd name="33" x="5.8" y="4" dx="1.524" dy="0.3302" layer="1"/>
<smd name="34" x="4" y="5.8" dx="0.3302" dy="1.524" layer="1"/>
<smd name="35" x="3.2" y="5.8" dx="0.3302" dy="1.524" layer="1"/>
<smd name="36" x="2.4" y="5.8" dx="0.3302" dy="1.524" layer="1"/>
<smd name="37" x="1.6" y="5.8" dx="0.3302" dy="1.524" layer="1"/>
<smd name="38" x="0.8" y="5.8" dx="0.3302" dy="1.524" layer="1"/>
<smd name="39" x="0" y="5.8" dx="0.3302" dy="1.524" layer="1"/>
<smd name="40" x="-0.8" y="5.8" dx="0.3302" dy="1.524" layer="1"/>
<smd name="41" x="-1.6" y="5.8" dx="0.3302" dy="1.524" layer="1"/>
<smd name="42" x="-2.4" y="5.8" dx="0.3302" dy="1.524" layer="1"/>
<smd name="43" x="-3.2" y="5.8" dx="0.3302" dy="1.524" layer="1"/>
<smd name="44" x="-4" y="5.8" dx="0.3302" dy="1.524" layer="1"/>
<text x="-4.064" y="6.858" size="1.016" layer="25">&gt;NAME</text>
<text x="-4.064" y="-1.7701" size="1.778" layer="27">&gt;VALUE</text>
<rectangle x1="-6.1001" y1="3.8001" x2="-4.95" y2="4.1999" layer="51"/>
<rectangle x1="-6.1001" y1="3" x2="-4.95" y2="3.4" layer="51"/>
<rectangle x1="-6.1001" y1="2.1999" x2="-4.95" y2="2.5999" layer="51"/>
<rectangle x1="-6.1001" y1="1.4" x2="-4.95" y2="1.8001" layer="51"/>
<rectangle x1="-6.1001" y1="0.5999" x2="-4.95" y2="1" layer="51"/>
<rectangle x1="-6.1001" y1="-0.1999" x2="-4.95" y2="0.1999" layer="51"/>
<rectangle x1="-6.1001" y1="-1" x2="-4.95" y2="-0.5999" layer="51"/>
<rectangle x1="-6.1001" y1="-1.8001" x2="-4.95" y2="-1.4" layer="51"/>
<rectangle x1="-6.1001" y1="-2.5999" x2="-4.95" y2="-2.1999" layer="51"/>
<rectangle x1="-6.1001" y1="-3.4" x2="-4.95" y2="-3" layer="51"/>
<rectangle x1="-6.1001" y1="-4.1999" x2="-4.95" y2="-3.8001" layer="51"/>
<rectangle x1="-4.1999" y1="-6.1001" x2="-3.8001" y2="-4.95" layer="51"/>
<rectangle x1="-3.4" y1="-6.1001" x2="-3" y2="-4.95" layer="51"/>
<rectangle x1="-2.5999" y1="-6.1001" x2="-2.1999" y2="-4.95" layer="51"/>
<rectangle x1="-1.8001" y1="-6.1001" x2="-1.4" y2="-4.95" layer="51"/>
<rectangle x1="-1" y1="-6.1001" x2="-0.5999" y2="-4.95" layer="51"/>
<rectangle x1="-0.1999" y1="-6.1001" x2="0.1999" y2="-4.95" layer="51"/>
<rectangle x1="0.5999" y1="-6.1001" x2="1" y2="-4.95" layer="51"/>
<rectangle x1="1.4" y1="-6.1001" x2="1.8001" y2="-4.95" layer="51"/>
<rectangle x1="2.1999" y1="-6.1001" x2="2.5999" y2="-4.95" layer="51"/>
<rectangle x1="3" y1="-6.1001" x2="3.4" y2="-4.95" layer="51"/>
<rectangle x1="3.8001" y1="-6.1001" x2="4.1999" y2="-4.95" layer="51"/>
<rectangle x1="4.95" y1="-4.1999" x2="6.1001" y2="-3.8001" layer="51"/>
<rectangle x1="4.95" y1="-3.4" x2="6.1001" y2="-3" layer="51"/>
<rectangle x1="4.95" y1="-2.5999" x2="6.1001" y2="-2.1999" layer="51"/>
<rectangle x1="4.95" y1="-1.8001" x2="6.1001" y2="-1.4" layer="51"/>
<rectangle x1="4.95" y1="-1" x2="6.1001" y2="-0.5999" layer="51"/>
<rectangle x1="4.95" y1="-0.1999" x2="6.1001" y2="0.1999" layer="51"/>
<rectangle x1="4.95" y1="0.5999" x2="6.1001" y2="1" layer="51"/>
<rectangle x1="4.95" y1="1.4" x2="6.1001" y2="1.8001" layer="51"/>
<rectangle x1="4.95" y1="2.1999" x2="6.1001" y2="2.5999" layer="51"/>
<rectangle x1="4.95" y1="3" x2="6.1001" y2="3.4" layer="51"/>
<rectangle x1="4.95" y1="3.8001" x2="6.1001" y2="4.1999" layer="51"/>
<rectangle x1="3.8001" y1="4.95" x2="4.1999" y2="6.1001" layer="51"/>
<rectangle x1="3" y1="4.95" x2="3.4" y2="6.1001" layer="51"/>
<rectangle x1="2.1999" y1="4.95" x2="2.5999" y2="6.1001" layer="51"/>
<rectangle x1="1.4" y1="4.95" x2="1.8001" y2="6.1001" layer="51"/>
<rectangle x1="0.5999" y1="4.95" x2="1" y2="6.1001" layer="51"/>
<rectangle x1="-0.1999" y1="4.95" x2="0.1999" y2="6.1001" layer="51"/>
<rectangle x1="-1" y1="4.95" x2="-0.5999" y2="6.1001" layer="51"/>
<rectangle x1="-1.8001" y1="4.95" x2="-1.4" y2="6.1001" layer="51"/>
<rectangle x1="-2.5999" y1="4.95" x2="-2.1999" y2="6.1001" layer="51"/>
<rectangle x1="-3.4" y1="4.95" x2="-3" y2="6.1001" layer="51"/>
<rectangle x1="-4.1999" y1="4.95" x2="-3.8001" y2="6.1001" layer="51"/>
</package>
<package name="QFN-44-7X7">
<description>&lt;b&gt;QFN-44&lt;/b&gt; (7x7x1.8mm)&lt;p&gt;
Source: http://www.st.com/stonline/products/literature/ds/11020.pdf</description>
<wire x1="-3.45" y1="3.45" x2="3.45" y2="3.45" width="0.1016" layer="51"/>
<wire x1="3.45" y1="3.45" x2="3.45" y2="-3.45" width="0.1016" layer="51"/>
<wire x1="3.45" y1="-3.45" x2="-3.45" y2="-3.45" width="0.1016" layer="51"/>
<wire x1="-3.45" y1="-3.45" x2="-3.45" y2="3.45" width="0.1016" layer="51"/>
<smd name="TH" x="0" y="0" dx="5.24" dy="5.24" layer="1" stop="no"/>
<smd name="1" x="-3.325" y="2.5" dx="0.8" dy="0.3" layer="1" stop="no" cream="no"/>
<smd name="2" x="-3.325" y="2" dx="0.8" dy="0.3" layer="1" stop="no" cream="no"/>
<smd name="3" x="-3.325" y="1.5" dx="0.8" dy="0.3" layer="1" stop="no" cream="no"/>
<smd name="4" x="-3.325" y="1" dx="0.8" dy="0.3" layer="1" stop="no" cream="no"/>
<smd name="5" x="-3.325" y="0.5" dx="0.8" dy="0.3" layer="1" stop="no" cream="no"/>
<smd name="6" x="-3.325" y="0" dx="0.8" dy="0.3" layer="1" stop="no" cream="no"/>
<smd name="7" x="-3.325" y="-0.5" dx="0.8" dy="0.3" layer="1" stop="no" cream="no"/>
<smd name="8" x="-3.325" y="-1" dx="0.8" dy="0.3" layer="1" stop="no" cream="no"/>
<smd name="9" x="-3.325" y="-1.5" dx="0.8" dy="0.3" layer="1" stop="no" cream="no"/>
<smd name="10" x="-3.325" y="-2" dx="0.8" dy="0.3" layer="1" stop="no" cream="no"/>
<smd name="11" x="-3.325" y="-2.5" dx="0.8" dy="0.3" layer="1" stop="no" cream="no"/>
<smd name="12" x="-2.5" y="-3.325" dx="0.8" dy="0.3" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="13" x="-2" y="-3.325" dx="0.8" dy="0.3" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="14" x="-1.5" y="-3.325" dx="0.8" dy="0.3" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="15" x="-1" y="-3.325" dx="0.8" dy="0.3" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="16" x="-0.5" y="-3.325" dx="0.8" dy="0.3" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="17" x="0" y="-3.325" dx="0.8" dy="0.3" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="18" x="0.5" y="-3.325" dx="0.8" dy="0.3" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="19" x="1" y="-3.325" dx="0.8" dy="0.3" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="20" x="1.5" y="-3.325" dx="0.8" dy="0.3" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="21" x="2" y="-3.325" dx="0.8" dy="0.3" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="22" x="2.5" y="-3.325" dx="0.8" dy="0.3" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="23" x="3.325" y="-2.5" dx="0.8" dy="0.3" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="24" x="3.325" y="-2" dx="0.8" dy="0.3" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="25" x="3.325" y="-1.5" dx="0.8" dy="0.3" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="26" x="3.325" y="-1" dx="0.8" dy="0.3" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="27" x="3.325" y="-0.5" dx="0.8" dy="0.3" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="28" x="3.325" y="0" dx="0.8" dy="0.3" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="29" x="3.325" y="0.5" dx="0.8" dy="0.3" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="30" x="3.325" y="1" dx="0.8" dy="0.3" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="31" x="3.325" y="1.5" dx="0.8" dy="0.3" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="32" x="3.325" y="2" dx="0.8" dy="0.3" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="33" x="3.325" y="2.5" dx="0.8" dy="0.3" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="34" x="2.5" y="3.325" dx="0.8" dy="0.3" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="35" x="2" y="3.325" dx="0.8" dy="0.3" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="36" x="1.5" y="3.325" dx="0.8" dy="0.3" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="37" x="1" y="3.325" dx="0.8" dy="0.3" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="38" x="0.5" y="3.325" dx="0.8" dy="0.3" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="39" x="0" y="3.325" dx="0.8" dy="0.3" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="40" x="-0.5" y="3.325" dx="0.8" dy="0.3" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="41" x="-1" y="3.325" dx="0.8" dy="0.3" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="42" x="-1.5" y="3.325" dx="0.8" dy="0.3" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="43" x="-2" y="3.325" dx="0.8" dy="0.3" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="44" x="-2.5" y="3.325" dx="0.8" dy="0.3" layer="1" rot="R270" stop="no" cream="no"/>
<text x="-3.2" y="4" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.2" y="-5.2" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.275" y1="3.05" x2="-3.05" y2="3.275" layer="1"/>
<rectangle x1="-3.775" y1="2.3" x2="-2.875" y2="2.7" layer="29"/>
<rectangle x1="-3.7" y1="2.375" x2="-2.95" y2="2.625" layer="31"/>
<rectangle x1="-3.775" y1="1.8" x2="-2.875" y2="2.2" layer="29"/>
<rectangle x1="-3.7" y1="1.875" x2="-2.95" y2="2.125" layer="31"/>
<rectangle x1="-3.775" y1="1.3" x2="-2.875" y2="1.7" layer="29"/>
<rectangle x1="-3.7" y1="1.375" x2="-2.95" y2="1.625" layer="31"/>
<rectangle x1="-3.775" y1="0.8" x2="-2.875" y2="1.2" layer="29"/>
<rectangle x1="-3.7" y1="0.875" x2="-2.95" y2="1.125" layer="31"/>
<rectangle x1="-3.775" y1="0.3" x2="-2.875" y2="0.7" layer="29"/>
<rectangle x1="-3.7" y1="0.375" x2="-2.95" y2="0.625" layer="31"/>
<rectangle x1="-3.775" y1="-0.2" x2="-2.875" y2="0.2" layer="29"/>
<rectangle x1="-3.7" y1="-0.125" x2="-2.95" y2="0.125" layer="31"/>
<rectangle x1="-3.775" y1="-0.7" x2="-2.875" y2="-0.3" layer="29"/>
<rectangle x1="-3.7" y1="-0.625" x2="-2.95" y2="-0.375" layer="31"/>
<rectangle x1="-3.775" y1="-1.2" x2="-2.875" y2="-0.8" layer="29"/>
<rectangle x1="-3.7" y1="-1.125" x2="-2.95" y2="-0.875" layer="31"/>
<rectangle x1="-3.775" y1="-1.7" x2="-2.875" y2="-1.3" layer="29"/>
<rectangle x1="-3.7" y1="-1.625" x2="-2.95" y2="-1.375" layer="31"/>
<rectangle x1="-3.775" y1="-2.2" x2="-2.875" y2="-1.8" layer="29"/>
<rectangle x1="-3.7" y1="-2.125" x2="-2.95" y2="-1.875" layer="31"/>
<rectangle x1="-3.775" y1="-2.7" x2="-2.875" y2="-2.3" layer="29"/>
<rectangle x1="-3.7" y1="-2.625" x2="-2.95" y2="-2.375" layer="31"/>
<rectangle x1="-2.95" y1="-3.525" x2="-2.05" y2="-3.125" layer="29" rot="R90"/>
<rectangle x1="-2.875" y1="-3.45" x2="-2.125" y2="-3.2" layer="31" rot="R90"/>
<rectangle x1="-2.45" y1="-3.525" x2="-1.55" y2="-3.125" layer="29" rot="R90"/>
<rectangle x1="-2.375" y1="-3.45" x2="-1.625" y2="-3.2" layer="31" rot="R90"/>
<rectangle x1="-1.95" y1="-3.525" x2="-1.05" y2="-3.125" layer="29" rot="R90"/>
<rectangle x1="-1.875" y1="-3.45" x2="-1.125" y2="-3.2" layer="31" rot="R90"/>
<rectangle x1="-1.45" y1="-3.525" x2="-0.55" y2="-3.125" layer="29" rot="R90"/>
<rectangle x1="-1.375" y1="-3.45" x2="-0.625" y2="-3.2" layer="31" rot="R90"/>
<rectangle x1="-0.95" y1="-3.525" x2="-0.05" y2="-3.125" layer="29" rot="R90"/>
<rectangle x1="-0.875" y1="-3.45" x2="-0.125" y2="-3.2" layer="31" rot="R90"/>
<rectangle x1="-0.45" y1="-3.525" x2="0.45" y2="-3.125" layer="29" rot="R90"/>
<rectangle x1="-0.375" y1="-3.45" x2="0.375" y2="-3.2" layer="31" rot="R90"/>
<rectangle x1="0.05" y1="-3.525" x2="0.95" y2="-3.125" layer="29" rot="R90"/>
<rectangle x1="0.125" y1="-3.45" x2="0.875" y2="-3.2" layer="31" rot="R90"/>
<rectangle x1="0.55" y1="-3.525" x2="1.45" y2="-3.125" layer="29" rot="R90"/>
<rectangle x1="0.625" y1="-3.45" x2="1.375" y2="-3.2" layer="31" rot="R90"/>
<rectangle x1="1.05" y1="-3.525" x2="1.95" y2="-3.125" layer="29" rot="R90"/>
<rectangle x1="1.125" y1="-3.45" x2="1.875" y2="-3.2" layer="31" rot="R90"/>
<rectangle x1="1.55" y1="-3.525" x2="2.45" y2="-3.125" layer="29" rot="R90"/>
<rectangle x1="1.625" y1="-3.45" x2="2.375" y2="-3.2" layer="31" rot="R90"/>
<rectangle x1="2.05" y1="-3.525" x2="2.95" y2="-3.125" layer="29" rot="R90"/>
<rectangle x1="2.125" y1="-3.45" x2="2.875" y2="-3.2" layer="31" rot="R90"/>
<rectangle x1="2.875" y1="-2.7" x2="3.775" y2="-2.3" layer="29" rot="R180"/>
<rectangle x1="2.95" y1="-2.625" x2="3.7" y2="-2.375" layer="31" rot="R180"/>
<rectangle x1="2.875" y1="-2.2" x2="3.775" y2="-1.8" layer="29" rot="R180"/>
<rectangle x1="2.95" y1="-2.125" x2="3.7" y2="-1.875" layer="31" rot="R180"/>
<rectangle x1="2.875" y1="-1.7" x2="3.775" y2="-1.3" layer="29" rot="R180"/>
<rectangle x1="2.95" y1="-1.625" x2="3.7" y2="-1.375" layer="31" rot="R180"/>
<rectangle x1="2.875" y1="-1.2" x2="3.775" y2="-0.8" layer="29" rot="R180"/>
<rectangle x1="2.95" y1="-1.125" x2="3.7" y2="-0.875" layer="31" rot="R180"/>
<rectangle x1="2.875" y1="-0.7" x2="3.775" y2="-0.3" layer="29" rot="R180"/>
<rectangle x1="2.95" y1="-0.625" x2="3.7" y2="-0.375" layer="31" rot="R180"/>
<rectangle x1="2.875" y1="-0.2" x2="3.775" y2="0.2" layer="29" rot="R180"/>
<rectangle x1="2.95" y1="-0.125" x2="3.7" y2="0.125" layer="31" rot="R180"/>
<rectangle x1="2.875" y1="0.3" x2="3.775" y2="0.7" layer="29" rot="R180"/>
<rectangle x1="2.95" y1="0.375" x2="3.7" y2="0.625" layer="31" rot="R180"/>
<rectangle x1="2.875" y1="0.8" x2="3.775" y2="1.2" layer="29" rot="R180"/>
<rectangle x1="2.95" y1="0.875" x2="3.7" y2="1.125" layer="31" rot="R180"/>
<rectangle x1="2.875" y1="1.3" x2="3.775" y2="1.7" layer="29" rot="R180"/>
<rectangle x1="2.95" y1="1.375" x2="3.7" y2="1.625" layer="31" rot="R180"/>
<rectangle x1="2.875" y1="1.8" x2="3.775" y2="2.2" layer="29" rot="R180"/>
<rectangle x1="2.95" y1="1.875" x2="3.7" y2="2.125" layer="31" rot="R180"/>
<rectangle x1="2.875" y1="2.3" x2="3.775" y2="2.7" layer="29" rot="R180"/>
<rectangle x1="2.95" y1="2.375" x2="3.7" y2="2.625" layer="31" rot="R180"/>
<rectangle x1="2.05" y1="3.125" x2="2.95" y2="3.525" layer="29" rot="R270"/>
<rectangle x1="2.125" y1="3.2" x2="2.875" y2="3.45" layer="31" rot="R270"/>
<rectangle x1="1.55" y1="3.125" x2="2.45" y2="3.525" layer="29" rot="R270"/>
<rectangle x1="1.625" y1="3.2" x2="2.375" y2="3.45" layer="31" rot="R270"/>
<rectangle x1="1.05" y1="3.125" x2="1.95" y2="3.525" layer="29" rot="R270"/>
<rectangle x1="1.125" y1="3.2" x2="1.875" y2="3.45" layer="31" rot="R270"/>
<rectangle x1="0.55" y1="3.125" x2="1.45" y2="3.525" layer="29" rot="R270"/>
<rectangle x1="0.625" y1="3.2" x2="1.375" y2="3.45" layer="31" rot="R270"/>
<rectangle x1="0.05" y1="3.125" x2="0.95" y2="3.525" layer="29" rot="R270"/>
<rectangle x1="0.125" y1="3.2" x2="0.875" y2="3.45" layer="31" rot="R270"/>
<rectangle x1="-0.45" y1="3.125" x2="0.45" y2="3.525" layer="29" rot="R270"/>
<rectangle x1="-0.375" y1="3.2" x2="0.375" y2="3.45" layer="31" rot="R270"/>
<rectangle x1="-0.95" y1="3.125" x2="-0.05" y2="3.525" layer="29" rot="R270"/>
<rectangle x1="-0.875" y1="3.2" x2="-0.125" y2="3.45" layer="31" rot="R270"/>
<rectangle x1="-1.45" y1="3.125" x2="-0.55" y2="3.525" layer="29" rot="R270"/>
<rectangle x1="-1.375" y1="3.2" x2="-0.625" y2="3.45" layer="31" rot="R270"/>
<rectangle x1="-1.95" y1="3.125" x2="-1.05" y2="3.525" layer="29" rot="R270"/>
<rectangle x1="-1.875" y1="3.2" x2="-1.125" y2="3.45" layer="31" rot="R270"/>
<rectangle x1="-2.45" y1="3.125" x2="-1.55" y2="3.525" layer="29" rot="R270"/>
<rectangle x1="-2.375" y1="3.2" x2="-1.625" y2="3.45" layer="31" rot="R270"/>
<rectangle x1="-2.95" y1="3.125" x2="-2.05" y2="3.525" layer="29" rot="R270"/>
<rectangle x1="-2.875" y1="3.2" x2="-2.125" y2="3.45" layer="31" rot="R270"/>
</package>
</packages>
<symbols>
<symbol name="ATXMEGA_A4U">
<wire x1="-20.32" y1="40.64" x2="22.86" y2="40.64" width="0.254" layer="94"/>
<wire x1="22.86" y1="40.64" x2="22.86" y2="-40.64" width="0.254" layer="94"/>
<wire x1="22.86" y1="-40.64" x2="-20.32" y2="-40.64" width="0.254" layer="94"/>
<wire x1="-20.32" y1="-40.64" x2="-20.32" y2="40.64" width="0.254" layer="94"/>
<text x="-20.32" y="-43.18" size="1.778" layer="96">&gt;VALUE</text>
<text x="-20.32" y="41.91" size="1.778" layer="95">&gt;NAME</text>
<pin name="PE0/SDA" x="-25.4" y="-38.1" length="middle"/>
<pin name="PE1/XCK0/SCL" x="-25.4" y="-35.56" length="middle"/>
<pin name="PE2/RXD0" x="-25.4" y="-33.02" length="middle"/>
<pin name="PE3/TXD0" x="-25.4" y="-30.48" length="middle"/>
<pin name="PD7/TXD1/SCK/DP" x="27.94" y="-20.32" length="middle" rot="R180"/>
<pin name="PD6/RXD1/MISO/DM" x="27.94" y="-22.86" length="middle" rot="R180"/>
<pin name="PD5/XCK1/MOSI" x="27.94" y="-25.4" length="middle" rot="R180"/>
<pin name="PD4/CS" x="27.94" y="-27.94" length="middle" rot="R180"/>
<pin name="PD3/TXD0" x="27.94" y="-30.48" length="middle" rot="R180"/>
<pin name="PD2/RXD0" x="27.94" y="-33.02" length="middle" rot="R180"/>
<pin name="PD1/XCK0" x="27.94" y="-35.56" length="middle" rot="R180"/>
<pin name="PD0" x="27.94" y="-38.1" length="middle" rot="R180"/>
<pin name="PC7/TXD1/SCK" x="27.94" y="2.54" length="middle" rot="R180"/>
<pin name="PC6/RXD1/MISO" x="27.94" y="0" length="middle" rot="R180"/>
<pin name="PC5/XCK1/MOSI" x="27.94" y="-2.54" length="middle" rot="R180"/>
<pin name="PC4/CS" x="27.94" y="-5.08" length="middle" rot="R180"/>
<pin name="PC3/TXD0" x="27.94" y="-7.62" length="middle" rot="R180"/>
<pin name="PC2/RXD0" x="27.94" y="-10.16" length="middle" rot="R180"/>
<pin name="PC1/SCL/XCK0" x="27.94" y="-12.7" length="middle" rot="R180"/>
<pin name="PC0/SDA" x="27.94" y="-15.24" length="middle" rot="R180"/>
<pin name="PB3" x="27.94" y="15.24" length="middle" rot="R180"/>
<pin name="PB2" x="27.94" y="12.7" length="middle" rot="R180"/>
<pin name="PB1" x="27.94" y="10.16" length="middle" rot="R180"/>
<pin name="PB0" x="27.94" y="7.62" length="middle" rot="R180"/>
<pin name="PA6" x="27.94" y="35.56" length="middle" rot="R180"/>
<pin name="PA7" x="27.94" y="38.1" length="middle" rot="R180"/>
<pin name="PA5" x="27.94" y="33.02" length="middle" rot="R180"/>
<pin name="PA4" x="27.94" y="30.48" length="middle" rot="R180"/>
<pin name="PA3" x="27.94" y="27.94" length="middle" rot="R180"/>
<pin name="PA2" x="27.94" y="25.4" length="middle" rot="R180"/>
<pin name="PA1" x="27.94" y="22.86" length="middle" rot="R180"/>
<pin name="PA0" x="27.94" y="20.32" length="middle" rot="R180"/>
<pin name="AVCC" x="-25.4" y="22.86" length="middle" direction="pwr"/>
<pin name="GND3" x="-25.4" y="7.62" visible="pad" length="middle" direction="pwr"/>
<pin name="VCC2" x="-25.4" y="17.78" visible="pad" length="middle" direction="pwr"/>
<pin name="PR0(XT2)" x="-25.4" y="-25.4" length="middle"/>
<pin name="PR1(XT1)" x="-25.4" y="-20.32" length="middle"/>
<pin name="VCC1" x="-25.4" y="15.24" visible="pad" length="middle" direction="pwr"/>
<pin name="VCC" x="-25.4" y="12.7" length="middle" direction="pwr"/>
<pin name="GND1" x="-25.4" y="2.54" visible="pad" length="middle" direction="pwr"/>
<pin name="GND" x="-25.4" y="0" length="middle" direction="pwr"/>
<pin name="PDI_DATA" x="-25.4" y="33.02" length="middle"/>
<pin name="GND2" x="-25.4" y="5.08" visible="pad" length="middle" direction="pwr"/>
<pin name="!RESET!/PDI_CLK" x="-25.4" y="38.1" length="middle"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ATXMEGA_A4U">
<gates>
<gate name="G$1" symbol="ATXMEGA_A4U" x="0" y="0"/>
</gates>
<devices>
<device name="TQFP-44-1-64" package="TQFP44-13THIN">
<connects>
<connect gate="G$1" pin="!RESET!/PDI_CLK" pad="35"/>
<connect gate="G$1" pin="AVCC" pad="39"/>
<connect gate="G$1" pin="GND" pad="8"/>
<connect gate="G$1" pin="GND1" pad="18"/>
<connect gate="G$1" pin="GND2" pad="30"/>
<connect gate="G$1" pin="GND3" pad="38"/>
<connect gate="G$1" pin="PA0" pad="40"/>
<connect gate="G$1" pin="PA1" pad="41"/>
<connect gate="G$1" pin="PA2" pad="42"/>
<connect gate="G$1" pin="PA3" pad="43"/>
<connect gate="G$1" pin="PA4" pad="44"/>
<connect gate="G$1" pin="PA5" pad="1"/>
<connect gate="G$1" pin="PA6" pad="2"/>
<connect gate="G$1" pin="PA7" pad="3"/>
<connect gate="G$1" pin="PB0" pad="4"/>
<connect gate="G$1" pin="PB1" pad="5"/>
<connect gate="G$1" pin="PB2" pad="6"/>
<connect gate="G$1" pin="PB3" pad="7"/>
<connect gate="G$1" pin="PC0/SDA" pad="10"/>
<connect gate="G$1" pin="PC1/SCL/XCK0" pad="11"/>
<connect gate="G$1" pin="PC2/RXD0" pad="12"/>
<connect gate="G$1" pin="PC3/TXD0" pad="13"/>
<connect gate="G$1" pin="PC4/CS" pad="14"/>
<connect gate="G$1" pin="PC5/XCK1/MOSI" pad="15"/>
<connect gate="G$1" pin="PC6/RXD1/MISO" pad="16"/>
<connect gate="G$1" pin="PC7/TXD1/SCK" pad="17"/>
<connect gate="G$1" pin="PD0" pad="20"/>
<connect gate="G$1" pin="PD1/XCK0" pad="21"/>
<connect gate="G$1" pin="PD2/RXD0" pad="22"/>
<connect gate="G$1" pin="PD3/TXD0" pad="23"/>
<connect gate="G$1" pin="PD4/CS" pad="24"/>
<connect gate="G$1" pin="PD5/XCK1/MOSI" pad="25"/>
<connect gate="G$1" pin="PD6/RXD1/MISO/DM" pad="26"/>
<connect gate="G$1" pin="PD7/TXD1/SCK/DP" pad="27"/>
<connect gate="G$1" pin="PDI_DATA" pad="34"/>
<connect gate="G$1" pin="PE0/SDA" pad="28"/>
<connect gate="G$1" pin="PE1/XCK0/SCL" pad="29"/>
<connect gate="G$1" pin="PE2/RXD0" pad="32"/>
<connect gate="G$1" pin="PE3/TXD0" pad="33"/>
<connect gate="G$1" pin="PR0(XT2)" pad="36"/>
<connect gate="G$1" pin="PR1(XT1)" pad="37"/>
<connect gate="G$1" pin="VCC" pad="9"/>
<connect gate="G$1" pin="VCC1" pad="19"/>
<connect gate="G$1" pin="VCC2" pad="31"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VFQFN-44" package="QFN-44-7X7">
<connects>
<connect gate="G$1" pin="!RESET!/PDI_CLK" pad="35"/>
<connect gate="G$1" pin="AVCC" pad="39"/>
<connect gate="G$1" pin="GND" pad="8 TH"/>
<connect gate="G$1" pin="GND1" pad="18"/>
<connect gate="G$1" pin="GND2" pad="30"/>
<connect gate="G$1" pin="GND3" pad="38"/>
<connect gate="G$1" pin="PA0" pad="40"/>
<connect gate="G$1" pin="PA1" pad="41"/>
<connect gate="G$1" pin="PA2" pad="42"/>
<connect gate="G$1" pin="PA3" pad="43"/>
<connect gate="G$1" pin="PA4" pad="44"/>
<connect gate="G$1" pin="PA5" pad="1"/>
<connect gate="G$1" pin="PA6" pad="2"/>
<connect gate="G$1" pin="PA7" pad="3"/>
<connect gate="G$1" pin="PB0" pad="4"/>
<connect gate="G$1" pin="PB1" pad="5"/>
<connect gate="G$1" pin="PB2" pad="6"/>
<connect gate="G$1" pin="PB3" pad="7"/>
<connect gate="G$1" pin="PC0/SDA" pad="10"/>
<connect gate="G$1" pin="PC1/SCL/XCK0" pad="11"/>
<connect gate="G$1" pin="PC2/RXD0" pad="12"/>
<connect gate="G$1" pin="PC3/TXD0" pad="13"/>
<connect gate="G$1" pin="PC4/CS" pad="14"/>
<connect gate="G$1" pin="PC5/XCK1/MOSI" pad="15"/>
<connect gate="G$1" pin="PC6/RXD1/MISO" pad="16"/>
<connect gate="G$1" pin="PC7/TXD1/SCK" pad="17"/>
<connect gate="G$1" pin="PD0" pad="20"/>
<connect gate="G$1" pin="PD1/XCK0" pad="21"/>
<connect gate="G$1" pin="PD2/RXD0" pad="22"/>
<connect gate="G$1" pin="PD3/TXD0" pad="23"/>
<connect gate="G$1" pin="PD4/CS" pad="24"/>
<connect gate="G$1" pin="PD5/XCK1/MOSI" pad="25"/>
<connect gate="G$1" pin="PD6/RXD1/MISO/DM" pad="26"/>
<connect gate="G$1" pin="PD7/TXD1/SCK/DP" pad="27"/>
<connect gate="G$1" pin="PDI_DATA" pad="34"/>
<connect gate="G$1" pin="PE0/SDA" pad="28"/>
<connect gate="G$1" pin="PE1/XCK0/SCL" pad="29"/>
<connect gate="G$1" pin="PE2/RXD0" pad="32"/>
<connect gate="G$1" pin="PE3/TXD0" pad="33"/>
<connect gate="G$1" pin="PR0(XT2)" pad="36"/>
<connect gate="G$1" pin="PR1(XT1)" pad="37"/>
<connect gate="G$1" pin="VCC" pad="9"/>
<connect gate="G$1" pin="VCC1" pad="19"/>
<connect gate="G$1" pin="VCC2" pad="31"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U$1" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="FAIRCHILD_500V_PMOS" device=""/>
<part name="GD_HI" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="UCC27511A_GATE_DRIVER" device="" value="UCC27511A"/>
<part name="GD_LO" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="UCC27511A_GATE_DRIVER" device="" value="UCC27511A"/>
<part name="U$4" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="ST_600V_NMOS" device=""/>
<part name="C_HI" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="CAPACITOR" device="2220" value="470n"/>
<part name="C_LO" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="CAPACITOR" device="2220" value="470n"/>
<part name="U$7" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="RESISTOR" device="" value="1M"/>
<part name="U$8" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="RESISTOR" device="" value="1M"/>
<part name="+250V" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="+48V" device="" value="+250V"/>
<part name="-250V" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="+48V" device="" value="-250V"/>
<part name="U$9" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="RESISTOR" device="" value="1"/>
<part name="U$10" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="RESISTOR" device="" value="1"/>
<part name="U$11" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="RESISTOR" device="" value="1"/>
<part name="U$12" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="RESISTOR" device="" value="1"/>
<part name="+12V1" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="+12V" device=""/>
<part name="+12V2" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="+12V" device=""/>
<part name="U$13" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="POWER_BARREL_JACK" device=""/>
<part name="U$14" library="fab" deviceset="ATXMEGA_A4U" device="TQFP-44-1-64" value="XMEGA"/>
<part name="IC1" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="REGULATOR" device="SOT23"/>
<part name="U$15" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="ZENER_ON_1SMA59XXBT3G" device="" value="15V"/>
<part name="U$16" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="ZENER_ON_1SMA59XXBT3G" device="" value="15V"/>
<part name="C4" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="CAPACITOR_ALUM" device=""/>
<part name="+12V3" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="+12V" device=""/>
<part name="GND1" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="GND" device=""/>
<part name="U$18" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="TAGCONNECT_PDI" device=""/>
<part name="LED1" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="DIODE_ES1F_FAIRCHILD" device="" value="GREEN"/>
<part name="LED2" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="DIODE_ES1F_FAIRCHILD" device="" value="ORANGE"/>
<part name="GND3" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="GND" device=""/>
<part name="GND4" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="GND" device=""/>
<part name="R1" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="RESISTOR" device="" value="10k"/>
<part name="+3V1" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="+3V3" device=""/>
<part name="+3V2" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="+3V3" device=""/>
<part name="C3" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="CAPACITOR" device="_0805_M" value="10u"/>
<part name="C2" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="CAPACITOR" device="_0805_M" value=".1u"/>
<part name="C1" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="CAPACITOR" device="_0805_M" value=".1u"/>
<part name="+12V5" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="+12V" device=""/>
<part name="U$17" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="HEADER_4POS_254" device="THIN" value="HEADER_4POS_254THIN"/>
<part name="GND2" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="GND" device=""/>
<part name="U$19" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="HEADER_3POS_254" device="THIN" value="HEADER_3POS_254THIN"/>
<part name="GND5" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="GND" device=""/>
<part name="+250V1" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="+48V" device="" value="+250V"/>
<part name="-250V1" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="+48V" device="" value="-250V"/>
<part name="D1" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="DIODE_HV_STTH112" device="" value="1200V"/>
<part name="D2" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="DIODE_HV_STTH112" device="" value="1200V"/>
<part name="GND6" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="GND" device=""/>
<part name="U$20" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="CAPACITOR" device="_0805_M" value="1u"/>
<part name="U$21" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="CAPACITOR" device="_0805_M" value=".1u"/>
<part name="U$22" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="CAPACITOR" device="_0805_M" value="1u"/>
<part name="U$23" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="CAPACITOR" device="_0805_M" value=".1u"/>
<part name="GND7" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="GND" device=""/>
<part name="GND8" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="GND" device=""/>
<part name="GND9" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="GND" device=""/>
<part name="GND10" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="GND" device=""/>
<part name="U$2" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="HOLE_M3" device=""/>
<part name="U$3" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="HOLE_M3" device=""/>
<part name="U$5" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="RESISTOR" device="" value="1k"/>
<part name="U$6" library="sec" library_urn="urn:adsk.eagle:library:725670" deviceset="RESISTOR" device="" value="1k"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U$1" gate="G$1" x="71.12" y="12.7"/>
<instance part="GD_HI" gate="G$1" x="-20.32" y="20.32" rot="R180"/>
<instance part="GD_LO" gate="G$1" x="-20.32" y="-20.32" rot="R180"/>
<instance part="U$4" gate="G$1" x="71.12" y="-27.94"/>
<instance part="C_HI" gate="C$1" x="15.24" y="20.32" rot="R90"/>
<instance part="C_LO" gate="C$1" x="17.78" y="-20.32" rot="R90"/>
<instance part="U$7" gate="G$1" x="35.56" y="-25.4" rot="R90"/>
<instance part="U$8" gate="G$1" x="35.56" y="25.4" rot="R90"/>
<instance part="+250V" gate="G$1" x="55.88" y="53.34"/>
<instance part="-250V" gate="G$1" x="55.88" y="-48.26" rot="R180"/>
<instance part="U$9" gate="G$1" x="2.54" y="20.32"/>
<instance part="U$10" gate="G$1" x="2.54" y="25.4"/>
<instance part="U$11" gate="G$1" x="2.54" y="-20.32"/>
<instance part="U$12" gate="G$1" x="2.54" y="-15.24"/>
<instance part="+12V1" gate="G$1" x="-2.54" y="17.78"/>
<instance part="+12V2" gate="G$1" x="-2.54" y="-22.86"/>
<instance part="U$13" gate="G$1" x="-152.4" y="38.1"/>
<instance part="U$14" gate="G$1" x="-104.14" y="-15.24"/>
<instance part="IC1" gate="G$1" x="-165.1" y="-10.16"/>
<instance part="U$15" gate="G$1" x="25.4" y="25.4" rot="R90"/>
<instance part="U$16" gate="G$1" x="27.94" y="-25.4" rot="R90"/>
<instance part="C4" gate="G$1" x="-182.88" y="50.8"/>
<instance part="+12V3" gate="G$1" x="-172.72" y="63.5"/>
<instance part="GND1" gate="1" x="-172.72" y="33.02"/>
<instance part="U$18" gate="G$1" x="-157.48" y="20.32" rot="R180"/>
<instance part="LED1" gate="G$1" x="-154.94" y="-40.64"/>
<instance part="LED2" gate="G$1" x="-154.94" y="-48.26"/>
<instance part="GND3" gate="1" x="-137.16" y="-22.86"/>
<instance part="GND4" gate="1" x="-170.18" y="7.62"/>
<instance part="R1" gate="G$1" x="-137.16" y="27.94" rot="R90"/>
<instance part="+3V1" gate="G$1" x="-137.16" y="40.64"/>
<instance part="+3V2" gate="G$1" x="-144.78" y="2.54"/>
<instance part="C3" gate="C$1" x="-144.78" y="-10.16"/>
<instance part="C2" gate="C$1" x="-154.94" y="-10.16"/>
<instance part="C1" gate="C$1" x="-180.34" y="-10.16"/>
<instance part="+12V5" gate="G$1" x="-180.34" y="2.54"/>
<instance part="U$17" gate="G$1" x="88.9" y="0"/>
<instance part="GND2" gate="1" x="73.66" y="-15.24"/>
<instance part="U$19" gate="G$1" x="-116.84" y="53.34"/>
<instance part="GND5" gate="1" x="-129.54" y="43.18"/>
<instance part="+250V1" gate="G$1" x="-124.46" y="63.5"/>
<instance part="-250V1" gate="G$1" x="-124.46" y="38.1" rot="R180"/>
<instance part="D1" gate="G$1" x="55.88" y="7.62" rot="R270"/>
<instance part="D2" gate="G$1" x="55.88" y="0" rot="R270"/>
<instance part="GND6" gate="1" x="-167.64" y="-53.34"/>
<instance part="U$20" gate="C$1" x="-2.54" y="-27.94"/>
<instance part="U$21" gate="C$1" x="-7.62" y="-27.94"/>
<instance part="U$22" gate="C$1" x="-2.54" y="12.7"/>
<instance part="U$23" gate="C$1" x="-7.62" y="12.7"/>
<instance part="GND7" gate="1" x="-53.34" y="20.32"/>
<instance part="GND8" gate="1" x="-50.8" y="-20.32"/>
<instance part="GND9" gate="1" x="-5.08" y="-35.56"/>
<instance part="GND10" gate="1" x="-5.08" y="5.08"/>
<instance part="U$2" gate="G$1" x="-81.28" y="53.34"/>
<instance part="U$3" gate="G$1" x="-71.12" y="53.34"/>
<instance part="U$5" gate="G$1" x="-162.56" y="-40.64"/>
<instance part="U$6" gate="G$1" x="-162.56" y="-48.26"/>
</instances>
<busses>
</busses>
<nets>
<net name="+12V" class="0">
<segment>
<pinref part="GD_LO" gate="G$1" pin="VDD"/>
<pinref part="+12V2" gate="G$1" pin="+48V"/>
<wire x1="-7.62" y1="-25.4" x2="-2.54" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="U$20" gate="C$1" pin="1"/>
<junction x="-2.54" y="-25.4"/>
<pinref part="U$21" gate="C$1" pin="1"/>
<junction x="-7.62" y="-25.4"/>
</segment>
<segment>
<pinref part="GD_HI" gate="G$1" pin="VDD"/>
<pinref part="+12V1" gate="G$1" pin="+48V"/>
<wire x1="-7.62" y1="15.24" x2="-2.54" y2="15.24" width="0.1524" layer="91"/>
<pinref part="U$22" gate="C$1" pin="1"/>
<junction x="-2.54" y="15.24"/>
<pinref part="U$23" gate="C$1" pin="1"/>
<junction x="-7.62" y="15.24"/>
</segment>
<segment>
<pinref part="U$13" gate="G$1" pin="VCC2"/>
<wire x1="-167.64" y1="50.8" x2="-172.72" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-172.72" y1="50.8" x2="-172.72" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$13" gate="G$1" pin="VCC1"/>
<wire x1="-172.72" y1="55.88" x2="-167.64" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-172.72" y1="55.88" x2="-172.72" y2="60.96" width="0.1524" layer="91"/>
<junction x="-172.72" y="55.88"/>
<pinref part="+12V3" gate="G$1" pin="+48V"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="-182.88" y1="53.34" x2="-182.88" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-182.88" y1="55.88" x2="-172.72" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="IN"/>
<pinref part="C1" gate="C$1" pin="1"/>
<wire x1="-172.72" y1="-7.62" x2="-180.34" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="+12V5" gate="G$1" pin="+48V"/>
<wire x1="-180.34" y1="0" x2="-180.34" y2="-7.62" width="0.1524" layer="91"/>
<junction x="-180.34" y="-7.62"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="U$4" gate="G$1" pin="G"/>
<pinref part="U$7" gate="G$1" pin="2"/>
<wire x1="38.1" y1="-20.32" x2="35.56" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="C_LO" gate="C$1" pin="2"/>
<wire x1="35.56" y1="-20.32" x2="27.94" y2="-20.32" width="0.1524" layer="91"/>
<junction x="35.56" y="-20.32"/>
<pinref part="U$16" gate="G$1" pin="C"/>
<wire x1="27.94" y1="-20.32" x2="22.86" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="27.94" y1="-22.86" x2="27.94" y2="-20.32" width="0.1524" layer="91"/>
<junction x="27.94" y="-20.32"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="G"/>
<pinref part="U$8" gate="G$1" pin="1"/>
<wire x1="38.1" y1="20.32" x2="35.56" y2="20.32" width="0.1524" layer="91"/>
<pinref part="C_HI" gate="C$1" pin="2"/>
<wire x1="20.32" y1="20.32" x2="25.4" y2="20.32" width="0.1524" layer="91"/>
<junction x="35.56" y="20.32"/>
<pinref part="U$15" gate="G$1" pin="A"/>
<wire x1="25.4" y1="20.32" x2="35.56" y2="20.32" width="0.1524" layer="91"/>
<wire x1="25.4" y1="22.86" x2="25.4" y2="20.32" width="0.1524" layer="91"/>
<junction x="25.4" y="20.32"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="GD_LO" gate="G$1" pin="OUTH"/>
<pinref part="U$11" gate="G$1" pin="1"/>
<wire x1="-7.62" y1="-20.32" x2="-2.54" y2="-20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="GD_LO" gate="G$1" pin="OUTL"/>
<pinref part="U$12" gate="G$1" pin="1"/>
<wire x1="-7.62" y1="-15.24" x2="-2.54" y2="-15.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="U$11" gate="G$1" pin="2"/>
<pinref part="C_LO" gate="C$1" pin="1"/>
<wire x1="7.62" y1="-20.32" x2="10.16" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="U$12" gate="G$1" pin="2"/>
<wire x1="10.16" y1="-20.32" x2="15.24" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="7.62" y1="-15.24" x2="10.16" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="10.16" y1="-15.24" x2="10.16" y2="-20.32" width="0.1524" layer="91"/>
<junction x="10.16" y="-20.32"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="GD_HI" gate="G$1" pin="OUTH"/>
<pinref part="U$9" gate="G$1" pin="1"/>
<wire x1="-7.62" y1="20.32" x2="-2.54" y2="20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="GD_HI" gate="G$1" pin="OUTL"/>
<pinref part="U$10" gate="G$1" pin="1"/>
<wire x1="-7.62" y1="25.4" x2="-2.54" y2="25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="U$9" gate="G$1" pin="2"/>
<pinref part="C_HI" gate="C$1" pin="1"/>
<wire x1="7.62" y1="20.32" x2="10.16" y2="20.32" width="0.1524" layer="91"/>
<pinref part="U$10" gate="G$1" pin="2"/>
<wire x1="10.16" y1="20.32" x2="12.7" y2="20.32" width="0.1524" layer="91"/>
<wire x1="7.62" y1="25.4" x2="10.16" y2="25.4" width="0.1524" layer="91"/>
<wire x1="10.16" y1="25.4" x2="10.16" y2="20.32" width="0.1524" layer="91"/>
<junction x="10.16" y="20.32"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="U$17" gate="G$1" pin="P$2"/>
<wire x1="86.36" y1="-2.54" x2="73.66" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="73.66" y1="-2.54" x2="73.66" y2="5.08" width="0.1524" layer="91"/>
<wire x1="73.66" y1="5.08" x2="55.88" y2="5.08" width="0.1524" layer="91"/>
<pinref part="D1" gate="G$1" pin="C"/>
<pinref part="D2" gate="G$1" pin="A"/>
<junction x="55.88" y="5.08"/>
<wire x1="55.88" y1="5.08" x2="55.88" y2="2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PULSE_LO" class="0">
<segment>
<pinref part="U$14" gate="G$1" pin="PD0"/>
<wire x1="-76.2" y1="-53.34" x2="-63.5" y2="-53.34" width="0.1524" layer="91"/>
<label x="-63.5" y="-53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="GD_LO" gate="G$1" pin="IN+"/>
<wire x1="-33.02" y1="-25.4" x2="-43.18" y2="-25.4" width="0.1524" layer="91"/>
<label x="-48.26" y="-25.4" size="1.778" layer="95"/>
</segment>
</net>
<net name="PULSE_HI" class="0">
<segment>
<pinref part="GD_HI" gate="G$1" pin="IN+"/>
<wire x1="-33.02" y1="15.24" x2="-43.18" y2="15.24" width="0.1524" layer="91"/>
<label x="-48.26" y="15.24" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$14" gate="G$1" pin="PE0/SDA"/>
<wire x1="-129.54" y1="-53.34" x2="-137.16" y2="-53.34" width="0.1524" layer="91"/>
<label x="-142.24" y="-53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="U$13" gate="G$1" pin="GND1"/>
<wire x1="-167.64" y1="45.72" x2="-172.72" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-172.72" y1="45.72" x2="-172.72" y2="40.64" width="0.1524" layer="91"/>
<pinref part="U$13" gate="G$1" pin="GND2"/>
<wire x1="-172.72" y1="40.64" x2="-167.64" y2="40.64" width="0.1524" layer="91"/>
<wire x1="-172.72" y1="40.64" x2="-172.72" y2="35.56" width="0.1524" layer="91"/>
<junction x="-172.72" y="40.64"/>
<pinref part="GND1" gate="1" pin="GND"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="-182.88" y1="45.72" x2="-182.88" y2="40.64" width="0.1524" layer="91"/>
<wire x1="-182.88" y1="40.64" x2="-172.72" y2="40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$14" gate="G$1" pin="GND3"/>
<wire x1="-129.54" y1="-7.62" x2="-137.16" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="-137.16" y1="-7.62" x2="-137.16" y2="-10.16" width="0.1524" layer="91"/>
<pinref part="U$14" gate="G$1" pin="GND"/>
<wire x1="-137.16" y1="-10.16" x2="-137.16" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="-137.16" y1="-12.7" x2="-137.16" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="-137.16" y1="-15.24" x2="-137.16" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="-129.54" y1="-15.24" x2="-137.16" y2="-15.24" width="0.1524" layer="91"/>
<junction x="-137.16" y="-15.24"/>
<pinref part="U$14" gate="G$1" pin="GND1"/>
<wire x1="-129.54" y1="-12.7" x2="-137.16" y2="-12.7" width="0.1524" layer="91"/>
<junction x="-137.16" y="-12.7"/>
<pinref part="U$14" gate="G$1" pin="GND2"/>
<wire x1="-129.54" y1="-10.16" x2="-137.16" y2="-10.16" width="0.1524" layer="91"/>
<junction x="-137.16" y="-10.16"/>
<pinref part="GND3" gate="1" pin="GND"/>
<pinref part="C1" gate="C$1" pin="2"/>
<wire x1="-180.34" y1="-15.24" x2="-165.1" y2="-15.24" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="GND"/>
<wire x1="-165.1" y1="-15.24" x2="-165.1" y2="-12.7" width="0.1524" layer="91"/>
<pinref part="C2" gate="C$1" pin="2"/>
<wire x1="-165.1" y1="-15.24" x2="-154.94" y2="-15.24" width="0.1524" layer="91"/>
<junction x="-165.1" y="-15.24"/>
<pinref part="C3" gate="C$1" pin="2"/>
<wire x1="-154.94" y1="-15.24" x2="-144.78" y2="-15.24" width="0.1524" layer="91"/>
<junction x="-154.94" y="-15.24"/>
<wire x1="-144.78" y1="-15.24" x2="-137.16" y2="-15.24" width="0.1524" layer="91"/>
<junction x="-144.78" y="-15.24"/>
</segment>
<segment>
<pinref part="U$18" gate="G$1" pin="GND"/>
<wire x1="-170.18" y1="12.7" x2="-170.18" y2="10.16" width="0.1524" layer="91"/>
<pinref part="GND4" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U$17" gate="G$1" pin="P$3"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="86.36" y1="-7.62" x2="73.66" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="73.66" y1="-7.62" x2="73.66" y2="-12.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$19" gate="G$1" pin="P$2"/>
<wire x1="-119.38" y1="50.8" x2="-129.54" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-129.54" y1="50.8" x2="-129.54" y2="45.72" width="0.1524" layer="91"/>
<pinref part="GND5" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="-167.64" y1="-40.64" x2="-167.64" y2="-48.26" width="0.1524" layer="91"/>
<pinref part="GND6" gate="1" pin="GND"/>
<pinref part="U$5" gate="G$1" pin="1"/>
<pinref part="U$6" gate="G$1" pin="1"/>
<wire x1="-167.64" y1="-48.26" x2="-167.64" y2="-50.8" width="0.1524" layer="91"/>
<junction x="-167.64" y="-48.26"/>
</segment>
<segment>
<pinref part="GD_HI" gate="G$1" pin="IN-"/>
<wire x1="-33.02" y1="20.32" x2="-35.56" y2="20.32" width="0.1524" layer="91"/>
<wire x1="-35.56" y1="20.32" x2="-35.56" y2="25.4" width="0.1524" layer="91"/>
<pinref part="GD_HI" gate="G$1" pin="GND"/>
<wire x1="-35.56" y1="25.4" x2="-33.02" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-35.56" y1="25.4" x2="-53.34" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-53.34" y1="25.4" x2="-53.34" y2="22.86" width="0.1524" layer="91"/>
<junction x="-35.56" y="25.4"/>
<pinref part="GND7" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GD_LO" gate="G$1" pin="IN-"/>
<wire x1="-33.02" y1="-20.32" x2="-35.56" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="-35.56" y1="-20.32" x2="-35.56" y2="-15.24" width="0.1524" layer="91"/>
<pinref part="GD_LO" gate="G$1" pin="GND"/>
<wire x1="-35.56" y1="-15.24" x2="-33.02" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="-35.56" y1="-15.24" x2="-50.8" y2="-15.24" width="0.1524" layer="91"/>
<junction x="-35.56" y="-15.24"/>
<wire x1="-50.8" y1="-15.24" x2="-50.8" y2="-17.78" width="0.1524" layer="91"/>
<pinref part="GND8" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U$20" gate="C$1" pin="2"/>
<pinref part="U$21" gate="C$1" pin="2"/>
<wire x1="-2.54" y1="-33.02" x2="-5.08" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="-5.08" y1="-33.02" x2="-7.62" y2="-33.02" width="0.1524" layer="91"/>
<junction x="-5.08" y="-33.02"/>
</segment>
<segment>
<pinref part="U$22" gate="C$1" pin="2"/>
<pinref part="U$23" gate="C$1" pin="2"/>
<wire x1="-2.54" y1="7.62" x2="-5.08" y2="7.62" width="0.1524" layer="91"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="-5.08" y1="7.62" x2="-7.62" y2="7.62" width="0.1524" layer="91"/>
<junction x="-5.08" y="7.62"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="U$18" gate="G$1" pin="CLK"/>
<pinref part="U$14" gate="G$1" pin="!RESET!/PDI_CLK"/>
<wire x1="-144.78" y1="22.86" x2="-137.16" y2="22.86" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="-137.16" y1="22.86" x2="-129.54" y2="22.86" width="0.1524" layer="91"/>
<junction x="-137.16" y="22.86"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="U$18" gate="G$1" pin="DAT"/>
<pinref part="U$14" gate="G$1" pin="PDI_DATA"/>
<wire x1="-144.78" y1="17.78" x2="-129.54" y2="17.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<pinref part="+3V1" gate="G$1" pin="+3V3"/>
<wire x1="-137.16" y1="33.02" x2="-137.16" y2="38.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="OUT"/>
<wire x1="-157.48" y1="-7.62" x2="-154.94" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="+3V2" gate="G$1" pin="+3V3"/>
<wire x1="-154.94" y1="-7.62" x2="-144.78" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="-144.78" y1="-7.62" x2="-144.78" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="C3" gate="C$1" pin="1"/>
<junction x="-144.78" y="-7.62"/>
<pinref part="C2" gate="C$1" pin="1"/>
<junction x="-154.94" y="-7.62"/>
<pinref part="U$14" gate="G$1" pin="VCC"/>
<wire x1="-144.78" y1="-2.54" x2="-144.78" y2="0" width="0.1524" layer="91"/>
<wire x1="-129.54" y1="-2.54" x2="-137.16" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="-137.16" y1="-2.54" x2="-137.16" y2="0" width="0.1524" layer="91"/>
<pinref part="U$18" gate="G$1" pin="VCC"/>
<wire x1="-137.16" y1="0" x2="-137.16" y2="2.54" width="0.1524" layer="91"/>
<wire x1="-137.16" y1="2.54" x2="-137.16" y2="7.62" width="0.1524" layer="91"/>
<wire x1="-137.16" y1="7.62" x2="-137.16" y2="12.7" width="0.1524" layer="91"/>
<wire x1="-137.16" y1="12.7" x2="-144.78" y2="12.7" width="0.1524" layer="91"/>
<pinref part="U$14" gate="G$1" pin="AVCC"/>
<wire x1="-129.54" y1="7.62" x2="-137.16" y2="7.62" width="0.1524" layer="91"/>
<junction x="-137.16" y="7.62"/>
<pinref part="U$14" gate="G$1" pin="VCC2"/>
<wire x1="-129.54" y1="2.54" x2="-137.16" y2="2.54" width="0.1524" layer="91"/>
<junction x="-137.16" y="2.54"/>
<pinref part="U$14" gate="G$1" pin="VCC1"/>
<wire x1="-129.54" y1="0" x2="-137.16" y2="0" width="0.1524" layer="91"/>
<junction x="-137.16" y="0"/>
<wire x1="-137.16" y1="-2.54" x2="-144.78" y2="-2.54" width="0.1524" layer="91"/>
<junction x="-137.16" y="-2.54"/>
<junction x="-144.78" y="-2.54"/>
</segment>
</net>
<net name="-250V" class="0">
<segment>
<pinref part="U$19" gate="G$1" pin="P$3"/>
<wire x1="-119.38" y1="45.72" x2="-124.46" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-124.46" y1="45.72" x2="-124.46" y2="40.64" width="0.1524" layer="91"/>
<pinref part="-250V1" gate="G$1" pin="+48V"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="S"/>
<pinref part="-250V" gate="G$1" pin="+48V"/>
<wire x1="55.88" y1="-45.72" x2="55.88" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="U$7" gate="G$1" pin="1"/>
<wire x1="55.88" y1="-38.1" x2="55.88" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="35.56" y1="-30.48" x2="35.56" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="35.56" y1="-38.1" x2="55.88" y2="-38.1" width="0.1524" layer="91"/>
<junction x="55.88" y="-38.1"/>
<pinref part="U$16" gate="G$1" pin="A"/>
<wire x1="27.94" y1="-27.94" x2="27.94" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="27.94" y1="-38.1" x2="35.56" y2="-38.1" width="0.1524" layer="91"/>
<junction x="35.56" y="-38.1"/>
<pinref part="U$17" gate="G$1" pin="P$4"/>
<wire x1="86.36" y1="-12.7" x2="83.82" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="83.82" y1="-12.7" x2="83.82" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="83.82" y1="-38.1" x2="55.88" y2="-38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+250V" class="0">
<segment>
<pinref part="U$19" gate="G$1" pin="P$1"/>
<wire x1="-119.38" y1="55.88" x2="-124.46" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-124.46" y1="55.88" x2="-124.46" y2="60.96" width="0.1524" layer="91"/>
<pinref part="+250V1" gate="G$1" pin="+48V"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="S"/>
<pinref part="+250V" gate="G$1" pin="+48V"/>
<wire x1="55.88" y1="35.56" x2="55.88" y2="40.64" width="0.1524" layer="91"/>
<pinref part="U$8" gate="G$1" pin="2"/>
<wire x1="55.88" y1="40.64" x2="55.88" y2="50.8" width="0.1524" layer="91"/>
<wire x1="35.56" y1="30.48" x2="35.56" y2="40.64" width="0.1524" layer="91"/>
<wire x1="35.56" y1="40.64" x2="55.88" y2="40.64" width="0.1524" layer="91"/>
<junction x="55.88" y="40.64"/>
<pinref part="U$15" gate="G$1" pin="C"/>
<wire x1="25.4" y1="27.94" x2="25.4" y2="40.64" width="0.1524" layer="91"/>
<wire x1="25.4" y1="40.64" x2="35.56" y2="40.64" width="0.1524" layer="91"/>
<junction x="35.56" y="40.64"/>
<pinref part="U$17" gate="G$1" pin="P$1"/>
<wire x1="86.36" y1="2.54" x2="83.82" y2="2.54" width="0.1524" layer="91"/>
<wire x1="83.82" y1="2.54" x2="83.82" y2="40.64" width="0.1524" layer="91"/>
<wire x1="83.82" y1="40.64" x2="55.88" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="D"/>
<pinref part="D1" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="U$4" gate="G$1" pin="D"/>
<pinref part="D2" gate="G$1" pin="C"/>
<wire x1="55.88" y1="-5.08" x2="55.88" y2="-2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LED2" class="0">
<segment>
<pinref part="LED2" gate="G$1" pin="C"/>
<wire x1="-152.4" y1="-48.26" x2="-144.78" y2="-48.26" width="0.1524" layer="91"/>
<label x="-144.78" y="-48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$14" gate="G$1" pin="PA4"/>
<wire x1="-76.2" y1="15.24" x2="-66.04" y2="15.24" width="0.1524" layer="91"/>
<label x="-71.12" y="15.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="LED1" class="0">
<segment>
<pinref part="LED1" gate="G$1" pin="C"/>
<wire x1="-152.4" y1="-40.64" x2="-144.78" y2="-40.64" width="0.1524" layer="91"/>
<label x="-144.78" y="-40.64" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$14" gate="G$1" pin="PA3"/>
<wire x1="-76.2" y1="12.7" x2="-66.04" y2="12.7" width="0.1524" layer="91"/>
<label x="-71.12" y="12.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="LED1" gate="G$1" pin="A"/>
<pinref part="U$5" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="LED2" gate="G$1" pin="A"/>
<pinref part="U$6" gate="G$1" pin="2"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
